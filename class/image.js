var fs = require('repository/fsPromise').fsPromise;
var filter = require('repository/fsPromise').filterPath;
var Dir = require('class/Directory');
var join = require('path').join;
var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = addImageToBase;

function addImageToBase(pathToFolderInImportWithImages,pathArtistImage) {
    this._pathToFolderWithArtistImage = join(pathArtistImage,'image');
    this._pathToFolderInImportWithImages = pathToFolderInImportWithImages;
    this._pathToArtistImage = '';
}

addImageToBase.prototype.distributeArtistImage = function(nameArtist) {
    return async(function() {
        var isExistFolderWithArtistImage = await(createDirForArtistImage.bind(this)());

        !isExistFolderWithArtistImage && await(moveArtistImage.bind(this)(nameArtist));
        return 'end';
    }).bind(this)();
};

function createDirForArtistImage() {
    return async(function() {

        var isExistFolderWithArtistImage = await(fs.exist(this._pathToFolderWithArtistImage));
        !isExistFolderWithArtistImage && await(fs.mkdir(this._pathToFolderWithArtistImage));
        return isExistFolderWithArtistImage;
    }).bind(this)();
}

function moveArtistImage(nameArtist) {
    return async(function() {
        var contentInFolderWithImages = await(fs.readdir(this._pathToFolderInImportWithImages));

        contentInFolderWithImages.map(function(content) {

            checkArtist(content,nameArtist) && await(hhh.bind(this)(content));

            return '';
        }.bind(this));
        return 'end';
    }).bind(this)();
}

function hhh(content) {
    return async(function() {

        var pathToFileInImport = join(this._pathToFolderInImportWithImages,content);

        await(fs.copy(pathToFileInImport,join(this._pathToFolderWithArtistImage,content)));
        await(fs.unlink(pathToFileInImport));

        this._pathToArtistImage = join(this._pathToFolderWithArtistImage,content);
    }).bind(this)();
}

function checkArtist(currentFileName,nameArtist) {
    var currentName = currentFileName.split('.')[0];
    console.log(currentName + '====' + nameArtist);
    var isArtistImage = currentName == nameArtist ? true : false;
    return isArtistImage;
}

addImageToBase.prototype.addDataToObject = function(artist,album,pathToAlbum,nameFolderWithContent) {
    return async(function() {

        artist.pathToImage = filter(this._pathToArtistImage,nameFolderWithContent);

        var contentInFolderWithImages = await(fs.readdir(this._pathToFolderInImportWithImages));
        contentInFolderWithImages.map(function(image) {

            album.pathToImage = filter(join(pathToAlbum,image),nameFolderWithContent);
        });
    }).bind(this)();

};

addImageToBase.prototype.moveAlbumImage = function(whereMove) {
    var dir = new Dir(whereMove);
    await(dir.moveFiles(this._pathToFolderInImportWithImages));
    console.log('55');
    await(dir.deleteDir(this._pathToFolderInImportWithImages));
};
