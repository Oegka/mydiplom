var async = require('asyncawait/async');
var await = require('asyncawait/await');
var array = require('lodash');

module.exports = Journal;

/**
 * Место для хранения изменений,которые были произведены задачей.
 * При добавление данных они распределяются по начальным состояниям. Так как данные уже могут находится в бд.
 * Поэтому изначально данные распределяются на созданные и к созданию.
 *
 * _nameDirWithMusic - название директории с музыкой
 * _areaBeforeDistribution - массив объектов до распределения
 * _existDirs - массив существующих директорий, которые не подлежат созданию и добавлению в базу
 * _dirsToCreate - массив объектов, которые подлежат к созданию
 * _createdDirs - массив созданных объектов
 * _deletedDirs - удаленные директории
 * _movedFiles - перемещенные файлы
 * _createdDataInBase - созаднные данные в базе.
 *
 * @param contentMusicDir
 * @constructor
 */
function Journal(contentMusicDir) {

    this._nameDirWithMusic = contentMusicDir;
    this._areaBeforeDistribution = [];

    this._existDirs = [];
    this._dirsToCreate = [];
    this._createdDirs = [];
    this._deletedDirs = [];
    this._movedFiles = [];

    this._createdDataInBase = [];
    this._fail = {};
}

Journal.prototype.add = function(objOfData) {
    this._areaBeforeDistribution.push({
        name: objOfData.name,
        rep: objOfData.rep,
        genre: objOfData.genre
    });
};

Journal.prototype.updateCreated = function(nameFolder,pathToDirInWhichCreated) {

    this._createdDirs.push({
        folder: nameFolder,
        inDir: pathToDirInWhichCreated,
        parentFolder:getParentFolder(pathToDirInWhichCreated),
        membershipCollection: toDetermineTheMembership(pathToDirInWhichCreated,this._nameDirWithMusic),
        nameDirWithMusic: this._nameDirWithMusic
    });
};

function toDetermineTheMembership(pathToDirInWhichCreated,rootDirName) {
    var parentFolder = getParentFolder(pathToDirInWhichCreated);

    if (parentFolder == rootDirName) {
        return 'artist';
    }
    else {
        return 'album';
    }
}

function getParentFolder(pathToDirInWhichCreated) {
    var dirs = pathToDirInWhichCreated.split('/');
    return array.last(dirs);
}

Journal.prototype.updateDeleted = function(pathDeletedDir) {
    this._deletedDirs.push({
        dir: pathDeletedDir
    });
};

Journal.prototype.updateMoved = function(movedFiles,membershipCollection) {
    movedFiles.forEach(function(file) {
        this._movedFiles.push({
            name: file.name,
            pathToTrack: file.pathToTrack,
            membershipCollection: membershipCollection
        })
    }.bind(this));
};

Journal.prototype.updateAddingDataToBase = function(data,newId) {
    this._createdDataInBase.push({
        nameCollection: data.nameCollection,
        createdId: newId,
        nameDocument: data.data.name
    })
};

Journal.prototype.initialDistributionState = function() {
    return async(function() {
        this._areaBeforeDistribution.map(function(objForAddingInJournal) {
            await(distribute(this,objForAddingInJournal));
            return 'end';
        }.bind(this));

    }.bind(this))();
};

function distribute(journal,objForAddingInJournal) {
    return async(function() {
        var repository = objForAddingInJournal.rep;
        var isExist = await(repository.findByName(objForAddingInJournal.name));

        if(!isExist) {

            journal._dirsToCreate.push(objForAddingInJournal.name);
        }
        else {

            journal._existDirs.push(objForAddingInJournal.name);
        }

        return 'end';
    })();

}

Journal.prototype.showJournal = function() {
    var show = this._existDirs.length != 0 ? this._exist : '0';
    console.log('Было уже создано :' + show);

    show = this._dirsToCreate.length != 0 ? this._toCreate : '0';
    console.log('Нужно было создать :' + show);

    show = this._createdDirs.length != 0 ? this._created : '0';
    showObj(show,'Было создано :');

    show = this._deletedDirs.length != 0 ? this._deleted : '0';
    showObj(show,'Было удалено :');
};

function showObj(show,message) {
    console.log(message);
    if (show== '0') {
        console.log(message + arrayObj);
    }
    else {
        show.forEach(function(obj) {
            for(key in obj) {
                console.log(key + ': ' + obj[key]);
            }
            console.log('\n');
        });
    }
}

