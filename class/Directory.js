
var fs = require('repository/fsPromise').fsPromise;
var join = require('path').join;
var ncp = require('ncp').ncp;
var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = Directory;

/**
 * Класс для работы с директориями
 * @param pathDir
 * путь до директории
 * @constructor
 */
function Directory(pathDir){

    this._path = pathDir;
}

Directory.prototype.createFolder = function(nameDir) {
    var pathNewFolder = join(this._path,nameDir);

    return fs.mkdir(pathNewFolder);

};

Directory.prototype.moveFiles = function(whence) {
    return new Promise(function(resolve,reject) {
        ncp(whence,this._path,function(err) {
            resolve();
        });
    }.bind(this));
};

Directory.prototype.deleteDir = function(pathDir) {
    var pathForDelete = '';
    if (pathDir === undefined) {
        pathForDelete = this._path;
    }
    else {
        pathForDelete = pathDir;
    }
    return async(function() {
        await(deleteFilesInDir(pathForDelete));
        console.log('YRAAA');
    })();


};

function deleteFilesInDir(path) {
    return async(function() {
        var filesInDir = await(fs.readdir(path));

        await(filesInDir.map(function(file) {
            return fs.unlink(join(path,file));
        }));

        await(fs.rmdir(path));

        return 'end';
    })();
}




