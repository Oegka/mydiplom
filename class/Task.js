var Directory = require('class/Directory');
var WorkImages = require('class/image');
var filter = require('repository/fsPromise').filterPath;
var repArtist = require('repository/repArtist');
var repAlbum = require('repository/repAlbum');
var repTrack = require('repository/repTrack');
var repParams = require('repository/parametersRep');
var Journal = require('class/journal');
var join = require('path').join;
var fs = require('repository/fsPromise').fsPromise;
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var config=require('config');
var mongoose = require('lib/mongoose');


module.exports = Task;


/**
 * Класс, который создает задачу для выполнения создания директорий и добавления информации в базу.
 * _objArtist - содержит название артиста,его жанр и репозиторий для работы с ним.
 * _objAlbum - содержит название альбома,его жанр,индефикатор артиста и репозиторий
 * _newTracks - содержит массив объектов, которые содержат название трека,путь до трека уже в конечном месте
 * т.е. где хранится музыка и репозиторий
 * _objRootDir - в этом объекте хранится вся начальная информация о директории импорта и директории, где хранится музыка
 * т.е. пути
 * _journal - переменная класса journal, в ней хранится вся информация о тех действиях,которые были выполнены задачей.
 * Таких как перемещение ,удаление,создание папок и таких как что добавлено было в базу.
 * Так же при добавление элементов в него он осущесвтляет распределение объекто по состояним.
 *
 *
 * @param nameFolderFromImport
 * Название папки ,в директории импорта, из которого извлекаются данные для распределения
 * по объектам коллекций
 * @param objRootDir
 * в этом объекте хранится вся начальная информация о директории импорта и директории, где хранится музыка
 * т.е. пути
 */
function Task(nameFolderFromImport,objRootDir) {

    this._nameFolderFromImport = nameFolderFromImport;

    this._objArtist = getDataArtist(nameFolderFromImport);
    this._objAlbum =  getDataAlbum(nameFolderFromImport);
    this._newTracks = [];

    this._path = '';
    this._objRootDir = objRootDir;
    this._journal = new Journal(objRootDir.CONTENT_MUSIC);

}

function getDataArtist(nameFolder) {
    var value = nameFolder.split('_');
    return {
        name: value[0],
        genre: [value[3]],
        rep: new repArtist(),
        pathToImage:''
    }
}

function getDataAlbum(nameFolder) {
    var value = nameFolder.split('_');
    return {
        name: value[1],
        genre: [value[3]],
        date: + value[2],
        artist: String,
        rep: new repAlbum(),
        pathToImage:''
    }
}

Task.prototype.createDirsToContentMusic = function() {

    return async(function() {


        await(addDataToLogStorageStatus(this._journal,[this._objArtist,this._objAlbum]));

        var pathToDirInWhichCreateFolder = getPathToDirInWhichNeedCreateFolder(this._journal._existDirs,this._objRootDir.fullCONTENT_MUSIC);
        var dir = new Directory(pathToDirInWhichCreateFolder);
        await(createFolders(this._journal,dir));

        var pathToTheFolderOfImport = join(this._objRootDir.fullIMPORT_DIR,this._nameFolderFromImport);

        await(addImagesToData.bind(this)(join(this._objRootDir.fullCONTENT_MUSIC,this._objArtist.name),pathToTheFolderOfImport,dir));

        await(addDataInObjTracks.bind(this)(this._newTracks,dir._path,pathToTheFolderOfImport));
        await(dir.moveFiles(pathToTheFolderOfImport));
        this._journal.updateMoved(this._newTracks,'track');

        await(dir.deleteDir(pathToTheFolderOfImport));
        this._journal.updateDeleted(pathToTheFolderOfImport);




    }).bind(this)();

};

function addDataToLogStorageStatus(journal,arrayOfData) {

    return async(function() {
        arrayOfData.forEach(function(dataForAddingInJournal) {
            journal.add(dataForAddingInJournal);
        });

        await(journal.initialDistributionState());

        return 'end';
    })();
}

function getPathToDirInWhichNeedCreateFolder(existDirs,pathToContentMusic) {
    var pathOfExistingFolders = '';
    existDirs.forEach(function(name) {
        pathOfExistingFolders = join(pathOfExistingFolders,name);
    });
    return join(pathToContentMusic,pathOfExistingFolders);
}

function addDataInObjTracks(newTracks,contentPath,importPath) {

    return async(function() {
        var contentFolderInImporthDirectory = await(fs.readdir(importPath));

        contentFolderInImporthDirectory.map(function(content) {
            newTracks.push({
                name: content,
                pathToTrack: filter(join(contentPath,content),this._objRootDir.CONTENT_MUSIC),
                artist: String,
                album: String,
                genre: this._objAlbum.genre[0],
                rep: new repTrack()
            });
        }.bind(this));
    }.bind(this))();
}

function addImagesToData(pathToImageArtist,importPath,dir) {
    return async(function() {

        var contentFolderInImporthDirectory = await(fs.readdir(importPath));

        contentFolderInImporthDirectory.map(function(content) {
            var stat = await(fs.stat(join(importPath,content)));
            if(stat.isDirectory()) {
                var workImages = new WorkImages(join(importPath,content),pathToImageArtist);
                await(workImages.distributeArtistImage(this._objArtist.name));

                await(workImages.addDataToObject(this._objArtist,this._objAlbum,join(dir._path,content),this._objRootDir.CONTENT_MUSIC));

                await(workImages.moveAlbumImage(join(dir._path,content)));

            }
        }.bind(this));

        return 'end';
    }).bind(this)();
}

function createFolders(journal,dir) {
    return async(function() {
        journal._dirsToCreate.map(function(nameFolder) {

            await(dir.createFolder(nameFolder));
            journal.updateCreated(nameFolder,dir._path);
            dir._path = join(dir._path,nameFolder);
        });
        return 'end';
    })();
}

Task.prototype.updateDatabase = function() {
    var dataCollections = {
        artist: this._objArtist,
        album: this._objAlbum,
        track: this._newTracks
    };

    return async(function() {
        await(addDataFromLogCreatedDirs(dataCollections,this._journal));
        await(addDataFromLogMovedFiles(dataCollections,this._journal));
        await(addGenreToArtist(dataCollections,this._journal._existDirs));

    }.bind(this))();

};

function addDataFromLogCreatedDirs(dataCollections,journal) {
    return async(function() {

        journal._createdDirs.map(function(informationAboutCreatedFolder) {

            var objForNewDocument = {
                nameCollection: informationAboutCreatedFolder.membershipCollection,
                data: dataCollections[informationAboutCreatedFolder.membershipCollection],
                journal: journal
            };

            await(createNewDocumentInBase(dataCollections,objForNewDocument));

        });
    })();
}

function createNewDocumentInBase(dataCollections,objForNewDocument) {


    return async(function() {
        await(ifExistParentsAddTheirId(objForNewDocument,dataCollections));

        var createdIdNewDocument = await(createDocument(objForNewDocument)).id;

        objForNewDocument.journal.updateAddingDataToBase(objForNewDocument,createdIdNewDocument);

    })();
}

function ifExistParentsAddTheirId(objForNewDocument,dataCollections) {

    return async(function() {
        var foundTheKeys = checkForParent(objForNewDocument.data);

        var objsParentsId = await(foundTheKeys.map(function(nameCollectionParent) {

            return getId(nameCollectionParent,dataCollections);
        }));


        addId(objForNewDocument.data,objsParentsId);
        return 'end';
    })();
}



function checkForParent(dataForNewDocument) {
    var parentsForNewDocument = [];
    for(key in dataForNewDocument) {
        if (key == 'artist' || key == 'album') {
            parentsForNewDocument.push(key);
        }
    }
    return parentsForNewDocument;
}

function getId(nameParentCollection,dataCollections) {
    return async(function() {
        var dataCollection = dataCollections[nameParentCollection];
        var nameDocument = dataCollection.name;


        var getId = getNameFunctionForCurrentCollection('getId',nameParentCollection);
        var repForParentCollection = dataCollection.rep;

        var id = await(repForParentCollection[getId](nameDocument));

        return {
            type: nameParentCollection,
            id: id
        };
    })();
}

function getNameFunctionForCurrentCollection(nameFunction,nameCollection) {

    return nameFunction + nameCollection[0].toUpperCase() + nameCollection.slice(1);
}

function addId(dataForNewDocument,objsParentsId) {
    objsParentsId.forEach(function(parentId) {

        dataForNewDocument[parentId.type] = parentId.id;

    });
}

function createDocument(objForNewDocument) {

    var dataForNewDocument = objForNewDocument.data;
    var repForDocument = objForNewDocument.data.rep;

    var create = getNameFunctionForCurrentCollection('create',objForNewDocument.nameCollection);
    try {
        return repForDocument[create](dataForNewDocument);
    }
    catch(err) {
        throw err;
    }
}

function addDataFromLogMovedFiles(dataCollections,journal) {
    return async(function() {

        await(dataCollections['track'].map(function(dataTrack) {
            return createNewDocumentInBase(dataCollections,{
                nameCollection: 'track',
                data: dataTrack,
                journal: journal
            });
        }));
    })();
}

function addGenreToArtist(data,existDirs) {
    return async(function() {
        console.log(data.artist.name + '65656');
        var artistRep = data.artist.rep;
        existDirs.length == 1 && await(artistRep.addGenre(data.artist.name,data.album.genre));
        return 'end';
    })();
}

Task.prototype.updateParamsCollections = function(genre) {
    return new Promise(function(resolve,reject) {
        var params = new repParams();
        var artists = new repArtist();
        var albums = new repAlbum();
        var tracks = new repTrack();

        params.updateGenres(genre)
            .then(function() {
                return artists.getCount();
            })
            .then(function(countArtists) {
                return params.updateCountArtists(countArtists);
            })
            .then(function() {
                return albums.getCount();
            })
            .then(function(countAlbums) {
                return params.updateCountAlbums(countAlbums);
            })
            .then(function() {
                return tracks.getCount();
            })
            .then(function(countAlbums) {
                return params.updateCountTracks(countAlbums);
            });
    }.bind(this));

};

