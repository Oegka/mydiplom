var Directory = require('repository/Directory');
var repArtist = require('repository/repArtist');
var repAlbum = require('repository/repAlbum');
var repTrack = require('repository/repTrack');
var Journal = require('repository/journal');
var join = require('path').join;
var fs = require('repository/fsPromise').fsPromise;
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var config=require('config');
var mongoose = require('lib/mongoose');

module.exports = createTask;

function createTask(FileName,objRootDir) {

    this._objArtist = getDataArtist(FileName);
    this._objAlbum =  getDataAlbum(FileName);
    this._objTrack = [];
    this._nameDir = FileName;
    this._path = '';
    this._objRootDir = objRootDir;
    this._journal = new Journal();



}

function getDataArtist(FileName) {
    var value = FileName.split('_');
    return {
        name: value[0],
        genre: [value[3]],
        rep: new repArtist()

    }

}

function getDataAlbum(FileName) {
    var value = FileName.split('_');
    return {
        name: value[1],
        genre: [value[3]],
        data: new Date(value[2],1,1),
        artist: String,
        rep: new repAlbum()

    }
}




createTask.prototype.createDirToContentMusic = function() {

    return async(function() {


        await(addDataInTheObservationLog(this._journal,[this._objArtist,this._objAlbum]));

        var initialPath = this._objRootDir.fullCONTENT_MUSIC + buildInitialPath(this._journal._exist);
        var dir = new Directory(initialPath);
        await(createFolders(this._journal,dir));

        var pathToTheFolderOfImport = join(this._objRootDir.fullIMPORT_DIR,this._nameDir);
        await(addDataInObjTracks(this._objTrack,dir._path,pathToTheFolderOfImport));
        await(dir.moveFiles(pathToTheFolderOfImport));
        this._journal.updateMoved(this._objTrack);

        await(dir.deleteDir(pathToTheFolderOfImport));
        this._journal.updateDeleted(pathToTheFolderOfImport);



    }).bind(this)();

};

function addDataInTheObservationLog(journal,arrayOfData) {

    return async(function() {
        arrayOfData.forEach(function(dataForAddingInJournal) {
            journal.add(dataForAddingInJournal);
        });

        await(journal.initialDistributionOfData());

        return 'end';

    })();
}

function buildInitialPath(exist) {
    var path = '';
    exist.forEach(function(name) {
        path = path + '/' + name;
    });
    return path;
}

function addDataInObjTracks(objTrack,contentPath,importPath) {

    return async(function() {
        var tracks = await(fs.readdir(importPath));

       tracks.map(function(currentTrackName) {
            objTrack.push({
                name: currentTrackName,
                pathToTrack: join(contentPath,currentTrackName),
                artist: String,
                album: String,
                rep: new repTrack()
            });
        });
    })();
}

function createFolders(journal,dir) {
    return async(function() {

        journal._toCreate.map(function(name) {
            await(dir.createFolder(name));
            journal.updateCreated(name,dir._path);
            dir._path = join(dir._path,name);


        });
        return 'end';
    })();
}

createTask.prototype.updateDatabase = function() {
    var dataCollections = {
        artist: this._objArtist,
        album: this._objAlbum,
        track: this._objTrack
    };

    return async(function() {
        await(addDataFromCreated(dataCollections,this._journal._created));
        await(addDataFromMoved(dataCollections,this._journal._moved));
    }.bind(this))();

};

function addDataFromCreated(dataCollections,created) {
    return async(function() {
        created.map(function(informationAboutCreatedFolder) {

            var objCollectionForAddingBase = {
                name: informationAboutCreatedFolder.type,
                data: dataCollections[informationAboutCreatedFolder.type]
            };
            await(createNewDocumentInBase(dataCollections,objCollectionForAddingBase));

        });
    })();

}

function createNewDocumentInBase(dataCollections,objCollectionForAddingBase) {


    return async(function() {
        await(ifExistParentsAddTheirId(objCollectionForAddingBase,dataCollections));

        await(createDocument(objCollectionForAddingBase));

    })();
}

function ifExistParentsAddTheirId(objCollectionForAddingBase,dataCollections) {

    return async(function() {
        var foundTheKeys = checkForParent(objCollectionForAddingBase.data);

        var objId = await(foundTheKeys.map(function(type) {
            return getId(type,dataCollections);
        }));


        addId(objCollectionForAddingBase.data,objId);
        return 'end';
    })();
}



function checkForParent(collection) {
    var parentsForCurrentCollection = [];
    for(key in collection) {
        if (key == 'artist' || key == 'album') {
            parentsForCurrentCollection.push(key);
        }
    }
    return parentsForCurrentCollection;
}

function getId(nameParentCollection,dataCollections) {
    return async(function() {
        var dataCollection = dataCollections[nameParentCollection];
        var nameDocument = dataCollection.name;

        var getId = getNameFunctionForCurrentCollection('getId',nameParentCollection);
        var repForCurrentCollection = dataCollection.rep;
        //console.log(nameParentCollection + '    ' + dataCollection.name);
        var id = await(repForCurrentCollection[getId](nameDocument));

        return {
            type: nameParentCollection,
            id: id
        };
    })();
}

function getNameFunctionForCurrentCollection(name,collection) {
    return name + collection[0].toUpperCase() + collection.slice(1);
}

function addId(collection,objId) {
    objId.forEach(function(obj) {
        console.log(obj.id);

        collection[obj.type] = obj.id;
        console.log(collection);
    });
}

function createDocument(objCollectionForAddingBase) {

    var dataForCurrentCollection = objCollectionForAddingBase.data;
    var repCurrentCollection = objCollectionForAddingBase.data.rep;
    var create = getNameFunctionForCurrentCollection('create',objCollectionForAddingBase.name);


    return repCurrentCollection[create](dataForCurrentCollection);




}

function addDataFromMoved(dataCollections) {
    return async(function() {
        console.log(dataCollections['track']);
        await(dataCollections['track'].map(function(currentDataOfTrack) {
            return createNewDocumentInBase(dataCollections,{
                name: 'track',
                data: currentDataOfTrack
            });
        }));
    })();
}