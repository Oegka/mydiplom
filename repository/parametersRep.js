var Parameters = require('models/paramsCollections');
var _ = require('lodash');

module.exports = parametersRepository;

function parametersRepository() {

}

parametersRepository.prototype.getCountGenres = function() {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                resolve(doc[0].genres.length);
            });
    });
};

parametersRepository.prototype.getGenres = function() {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                resolve(doc[0].genres);
            });
    });
};

parametersRepository.prototype.getCountArtists = function() {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                resolve(doc[0].countArtists);
            });
    });
};

parametersRepository.prototype.getCountAlbums = function() {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                resolve(doc[0].countAlbums);
            });
    });
};

parametersRepository.prototype.getCountTracks = function() {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                resolve(doc[0].countTracks);
            });
    });
};

parametersRepository.prototype.updateGenres = function(newGenres) {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                doc[0].genres = _.union(doc[0].genres,newGenres);
                return doc[0].save();
            })
            .then(function(doc) {
                resolve();
            });
    });
};

parametersRepository.prototype.findByGenre = function(genre) {
    return new Promise(function(resolve,reject) {
        Parameters.findOne({genres: genre})
            .then(function(doc) {
                resolve(doc);
            });
    });
};

parametersRepository.prototype.updateCountArtists = function(newCountArtists) {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                if (doc[0].countArtists != newCountArtists) {
                    doc[0].countArtists = newCountArtists;
                    return doc[0].save();
                }
                return '';
            })
            .then(function() {
                resolve();
            });
    });
};

parametersRepository.prototype.updateCountAlbums = function(newCountAlbums) {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                if (doc[0].countAlbums != newCountAlbums) {
                    doc[0].countAlbums = newCountAlbums;
                    return doc[0].save();
                }
                return '';
            })
            .then(function() {
                resolve();
            });
    });
};

parametersRepository.prototype.updateCountTracks = function(newCountTracks) {
    return new Promise(function(resolve,reject) {
        Parameters.find()
            .then(function(doc) {
                if (doc[0].countTracks != newCountTracks) {
                    doc[0].countTracks = newCountTracks;
                    return doc[0].save();
                }
                return '';
            })
            .then(function() {
                resolve();
            });
    });
};