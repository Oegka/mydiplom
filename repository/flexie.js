

module.exports = new flex();

function flex() {

}

//---------------------------------------------------------------------------------------------------------------------

flex.prototype.unionProps = function(obj_one,obj_two) {
    var copy_one = this.copyObject(obj_one);
    var copy_two = this.copyObject(obj_two);

    Object.keys(obj_two).map(function(key) {
        copy_one[key] = copy_two[key];
    });

    return copy_one;
};

//---------------------------------------------------------------------------------------------------------------------
flex.prototype.copyObject = function(objForCopy) {
    var clon = {};

    Object.keys(objForCopy).map(function(key) {
        clon[key] = objForCopy[key];
    });
    return clon;
};

//---------------------------------------------------------------------------------------------------------------------
flex.prototype.copyArray = function(array) {
    var newArray = array.map(function(element) {
        return element;
    });
    return newArray;
};