var Track = require('models/Track.js');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var url = require('url');
var array = require('lodash');


module.exports = tracksRepository;

function tracksRepository() {
}

tracksRepository.prototype.findAll = function(params,callback) {

    if (!params.sort) {
        params.sort = 'name';
    }

    if (!params.limit) {
        params.limit = 6;
    }

    if (!params.head) {
        params.head = 0;
    }

    if (!params.sortMode) {
        params.sortMode = 'min';
    }


    return Track.find(params.query)
        .then(function(doc) {
            return Track.sort(params,doc);
        })
        .then(function(doc) {
            return Track.limit(params,doc);
        })
        .then(function(doc) {
            return doc;
        })
        .catch(function(err) {
           console.log(err);
        });
};

tracksRepository.prototype.findByName = function(name,callback) {
    return Track.findOne({name: name})
        .then(function(doc) {
            if (callback === undefined) {
                return doc;
            }
            else {
                return callback(null,doc);
            }
        });
};

tracksRepository.prototype.getPathToTrack = function(name,callback) {
    return this.findByName(name,function(err,doc) {
        return Promise.resolve(doc);
    })
        .then(function(doc) {
            if (callback === undefined) {
                return doc.pathToTrack;
            }
            else {
                return callback(null,doc);
            }
        })
};

tracksRepository.prototype.createTrack = function(objTrack,callback) {

    var track = new Track();
    track.name = objTrack.name;
    track.artist = objTrack.artist;
    track.album = objTrack.album;
    track.pathToTrack = objTrack.pathToTrack;
    track.genre = objTrack.genre;

    return new Promise(function(resolve,reject) {
        return track.save()
            .then(function(doc) {

                return Track.findById(doc);
            })
            .then(function(doc) {
                if (callback === undefined) {
                    resolve(doc);
                }
                else {

                    reject();
                }
            });
    });

};

tracksRepository.prototype.findById = function(track,callback) {

    return Track.findOne({id:track.id})
        .then(function(doc) {
            if (callback === undefined) {
                return Promise.resolve(doc);
            }
            else {
                return callback(null,doc);
            }
        })
        .catch(function(err) {
            return callback(err);
        });
};

tracksRepository.prototype.findByIdAlbum = function(id,next) {
    return new Promise(function(resolve,reject) {
        Track.find({album: id})
            .then(function(doc) {
                console.log(doc + '666');
                if (doc.length != 0) {
                    resolve(doc);
                }

            })
            .catch(function(err) {
                console.log(err);
            })
    })
};

tracksRepository.prototype.getCount = function() {
    return new Promise(function(resolve,reject) {
        Track.count()
            .then(function(count) {
                resolve(count);
            });
    });
};