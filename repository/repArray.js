var async=require('asyncawait/async');
var await=require('asyncawait/await');
var as=require('async');

exports.copyArray=function(newArray,arrayToCopy,callback) {
    newArray.splice(0,newArray.length);
    if(arrayToCopy.length!=0) {
        async.each(arrayToCopy,function(doc,callback) {
            newArray.push(doc);
            callback(null);
        },function(err) {
            callback(null);
        });
    }
    else {
        callback(null);
    }
};

//---------------------------------------------------------------------------------------------------------------------
/**
 * Поиск индексов вхождения объекта в массив
 *
 * @param array
 * Массив,в котором осуществляется поиск
 *
 * @param searchObject
 * Объект, для которого происходит поиск
 *
 * @param callback
 * Функция, которая должна вызваться по окончанию поиска индексов
 */

exports.indexOfObject=function(array,searchObject,callback) {
    var matchesIndex=[];
    var i=-1;
    as.each(array,function(doc,callback) {
        i++;
        splitEach(doc,searchObject,matchesIndex,i,callback);
    },function(err) {
        callback(matchesIndex);
    });
};

function splitEach(doc,searchObject,matches,count,callback) {
    as.each(Object.keys(searchObject),function(key,callback) {
        if(doc[key]==searchObject[key]) {
            matches.push(count);
            callback(null);
        }
        else {
            callback(new Error());
        }
    },function(err) {
        callback(null);
    });
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Ищет поиск соответствий между двумя массивами
 *
 * Доп. информация
 * Поиск осуществляется за счет сравнения объектов из двух массивов по уникальному ключу.
 * В данном случае, испольузется имя артиста, так как это уникальный ключ.
 * Возвращается массив из одинаковых объектов.
 *
 * @param property
 * Задаем по какому ключу идет сравнение
 *
 * @param array
 * Первый массив для поиска совпадений
 *
 * @param searchObjects
 * Второй массив для поиска совпадений
 *
 * @param callback
 */

exports.getTheSameObject=function(property,array,searchObjects,callback) {
    var arrayMatches=[];

    as.each(array,function(doc,callback) {
        splitEachForUniqueProperty(doc,searchObjects,property,arrayMatches,callback);

    },function(err) {
        callback(arrayMatches);
    });
};

function splitEachForUniqueProperty(doc,searchObjects,property,arrayMatches,callback) {
    as.each(searchObjects,function(object,callback) {

        if(doc[property]==object[property]) {
            arrayMatches.push(object);
            callback(null);
        }
        else {
            callback(null);
        }
    },function(err) {
        callback(null);
    })
}

//---------------------------------------------------------------------------------------------------------------------
function checked (property,docFirst,docLast) {
    var flag=false;
    if(docFirst[property]== docLast[property]) {
        flag=true;
    }
    return flag;
}

function addElement(same,matches,docLast) {
    if(same) {
        matches.push(docLast);

    }

}

var compareElements=async(function(property,docFirst,docLast,matches) {
    //console.log(docLast);
    var same= await(checked(property,docFirst,docLast));
    await(addElement(same,matches,docLast));
    return 'end';

});

var getSecondElement=async(function(property,docFirst,searchObjects,matches) {
    return await(searchObjects.forEach(function(doc) {
        compareElements(property,docFirst,doc,matches);
    }));
});

var get=async(function(property,array,searchObjects,matches) {
    return await(array.forEach(function(doc) {
        getSecondElement(property,doc,searchObjects,matches);
    }));
});

exports.TheSameObject=function(property,array,searchObjects,callback) {
    var arrMatches=[];

    get(property,array,searchObjects,arrMatches)
        .then(function(matches) {
            console.log(matches);
            callback(arrMatches);
        });
};