var fs = require('fs');
var extra = require('fs-extra');

exports.fsPromise = new fsPromise();

function fsPromise() {
}

fsPromise.prototype.readdir = function(path) {
    return new Promise(function(resolve,reject) {
        fs.readdir(path,function(err,item) {
            if (err) reject(err);
            resolve(item);

        });
    });
};

fsPromise.prototype.unlink = function(path) {
    return new Promise(function(resolve,reject) {
        fs.unlink(path,function(err) {
            if(err) throw err;
            resolve();
        });
    });
};

fsPromise.prototype.rmdir = function(path) {
    return new Promise(function(resolve,reject) {
        fs.rmdir(path,function(err) {
            if (err) reject(err);
            resolve();
        });
    });
};

fsPromise.prototype.mkdir = function(path) {
    return new Promise(function(resolve,reject) {
        fs.mkdir(path,function() {

            resolve();
        });
    });
};

fsPromise.prototype.exist = function(path) {

    return new Promise(function(resolve,reject) {
        fs.exists(path,function(ex) {

            resolve(ex);
        });
    });
};

fsPromise.prototype.stat = function(path) {
    return new Promise(function(resolve,reject) {
        fs.stat(path,function(err,stat) {
            if(err) throw err;
            resolve(stat);
        });
    });
};

fsPromise.prototype.rename = function(whence,where) {

    return new Promise(function(resolve,reject) {
        fs.rename(whence,where,function(err) {
            if(err) throw err;
            resolve();
        });
    });
};

fsPromise.prototype.copy = function(whence,where) {
    console.log('need' + whence + where);
    return new Promise(function(resolve,rejet) {
        extra.copy(whence,where,function(err) {
            if(err) throw err;
            resolve();
        });
    });
};

exports.filterPath = function(pathForProcessing,nameFolderWithContent) {

    var index = pathForProcessing.indexOf(nameFolderWithContent);
    var lengthNameFolderWithContent = nameFolderWithContent.length;
    var path = pathForProcessing.slice(index+lengthNameFolderWithContent+1);
    return path;
};