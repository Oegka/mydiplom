var Album = require('models/Album.js');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var url = require('url');
var array = require('lodash');

module.exports = albumsRepository;

function albumsRepository() {
}

albumsRepository.prototype.findAll = function(params,callback) {

    return new Promise(function(resolve,reject) {

        if (!params.sort) {
            params.sort = 'name';
        }

        if (!params.limit) {
            params.limit = 6;
        }

        if (!params.head) {
            params.head = 0;
        }

        if (!params.sortMode) {
            params.sortMode = 'min';
        }

        Album.find(params.query)
            .then(function(doc) {
                return Album.sort(params,doc);
            })
            .then(function(doc) {
                return Album.limit(params,doc);

            })
            .then(function(doc) {
                resolve(doc);
            })
            .catch(function(err) {
                reject(err);
            });
    });
};

albumsRepository.prototype.createAlbum = function(objAlbum,callback) {

    var album = new Album();
    album.name = objAlbum.name;
    album.genreAlbum = objAlbum.genre;
    album.DateOfIssue = new Date(objAlbum.date,1,1);
    album.artist = objAlbum.artist;
    album.pathToImage = objAlbum.pathToImage;
    album.createDate = new Date();

    return new Promise(function(resolve,reject) {
        album.save()
            .then(function(doc) {

                return Album.findById(doc);
            })
            .then(function(doc) {
                if (callback === undefined) {
                    resolve(doc);
                }
                else {
                    //return callback(null,doc);
                    reject();
                }

            });
    });

};

albumsRepository.prototype.findById = function(id,callback) {

    return Album.findOne({_id:id})
        .then(function(doc) {
            if (callback === undefined) {
                return Promise.resolve(doc);
            }
            else {
                return callback(null,doc);
            }
        })
        .catch(function(err) {
            return callback(err);
        });

};

albumsRepository.prototype.findByGenre = function(genre,callback) {

    return Album.find({genre:genre})
        .then(function(doc) {
            return callback(null,doc);
        })
        .catch(function(err) {
            return callback(err);
        });
};

albumsRepository.prototype.findByDate = function(year,callback) {
    return Album.find({DateOfIssue: new Date(year,1,1)})
        .then(function(doc) {
            return callback(null,doc);
        })
        .catch(function(err) {
            return callback(err);
        });
};

albumsRepository.prototype.getIdAlbum = function(name,callback) {
    return this.findByName(name)
        .then(function(doc) {
            if(callback === undefined) {
                return Promise.resolve(doc.id);
            }
            else {
                return callback(null,doc.id);
            }
        });
};

albumsRepository.prototype.findByName = function(name,callback) {

    return new Promise(function(resolve,reject) {
        Album.findOne({name:name})
            .then(function(doc) {
                if (doc) {
                    resolve(doc);
                }
                else {
                    resolve(false);
                }
            })
            .catch(function(err) {
                console.log(err);
            });
    });

};

albumsRepository.prototype.findMatches = function(word,params) {

    return new Promise(function(resolve,reject) {

        this.findAll(params)
            .then(function(doc) {

                resolve(getMatches(doc,word));
            })
            .catch(function(err) {
                console.log(err);
            });
    }.bind(this));
};

function getMatches(doc,word) {
    var changeHead = 0;
    var matches = [];
    var index = 0;

    while(index < doc.length) {
        var isMatch = CheckMatch(doc[index].name,word);

        if (!isMatch && changeHead == 1) {
            break;
        }
        else {
            if (isMatch) {
                matches.push({
                    title: doc[index].name,
                    rep: 'album'
                });
                changeHead = 1;
            }
        }
        index ++;
    }
    return matches;
}

function CheckMatch(name,word) {

    return name.indexOf(word) == 0 ? true : false;
}

albumsRepository.prototype.getCount = function() {
    return new Promise(function(resolve,reject) {
        Album.count()
            .then(function(count) {
                resolve(count);
            });
    });
};