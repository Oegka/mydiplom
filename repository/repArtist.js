var Artist = require('models/Artist.js');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var url = require('url');
var array = require('lodash');
var HttpError = require('error/HttpError');
ObjectID = require('mongodb').ObjectID;


module.exports = artistRepository;

function artistRepository() {
}

artistRepository.prototype.findAll = function(params,next) {

    if (!params.sort) {
        params.sort = 'name';
    }

    if(!params.limit) {
        params.limit = 10;
    }

    if(!params.head) {
        params.head = 0;
    }

    if (!params.sortMode) {
        params.sortMode = 'min'
    }

    return Artist.find(params.query)
        .then(function(doc) {
            return Artist.sort(params,doc);
        })
        .then(function(doc) {
            return Artist.limit(params,doc);

        })
        .then(function(doc) {
            return Promise.resolve(doc);
        })
        .catch(function(err) {
            console.log(err);
        });

};

artistRepository.prototype.findByName = function(name,next) {

    return Artist.findOne({name:name})
        .then(function(doc) {
            if(doc) {
                return Promise.resolve(doc);
            }
            else {
                if(next === undefined) {

                    return Promise.resolve(doc);
                }
                else {
                    next(new HttpError(404,'Артист с именем ' + name + ' отсутствует'));
                }

            }
        })
        .catch(function(err) {
            //next(err);
            throw err;
        });
};

artistRepository.prototype.findByGenre = function(genre,next) {

    return Artist.find({genre:genre})
        .then(function(doc) {
            if(doc.length != 0) {
                return Promise.resolve(doc);
            }
            else {
                if(next === undefined) {

                    throw new Error();
                }
                else {
                    next(new HttpError(404,'Артисты с жанром ' + name + ' отсутствуют'));
                }
            }
        })
        .catch(function(err) {
            next(err);
        });
};

artistRepository.prototype.findById = function(id,next) {
    try {
        var objectId = new ObjectID(id);
    }
    catch(err) {
        return next(404);
    }

    return Artist.findOne({_id: objectId})
        .then(function(doc) {
            return Promise.resolve(doc);
        },function(err) {
            return next(err);
        })
};

artistRepository.prototype.createArtist = function(objArtist,next) {

    var artist = new Artist();
    artist.name = objArtist.name;
    artist.genre = array.union(objArtist.genre,artist.genre);
    artist.pathToImage = objArtist.pathToImage;

    return new Promise(function(resolve,reject) {
        artist.save()
            .then(function(doc) {

                return Artist.findById(doc);
            })
            .then(function(doc) {
                resolve(doc);
            },function(err) {
                //return next(err);
                reject(err);

            });
    });


};

artistRepository.prototype.getIdArtist = function(name,callback) {
    return this.findByName(name)
        .then(function(doc) {


            return Promise.resolve(doc.id);

        },function(err) {
            throw err;
        });
};

artistRepository.prototype.getAttribute = function() {
    var artist = new Artist();
    var arrayAttribute = [];

    return Promise.all(Object.keys(artist.schema.paths).map(function(currentAttributeModel) {
        return {
            path: currentAttributeModel,
            options: artist.schema.paths[currentAttributeModel].options
        };
    }));
};

artistRepository.prototype.getImageById = function(id) {

    return new Promise(function(resolve,reject) {

        this.findById({_id: id})
            .then(function(doc) {

                resolve(doc.pathToImage);
            });
    }.bind(this));
};

artistRepository.prototype.findMatches = function(word,params) {

    return new Promise(function(resolve,reject) {

        this.findAll(params)
            .then(function(doc) {

                resolve(getMatches(doc,word));
            })
            .catch(function(err) {
                console.log(err);
            });
    }.bind(this));
};

function getMatches(doc,word) {
    var changeHead = 0;
    var matches = [];
    var index = 0;

    while(index < doc.length) {
        var isMatch = CheckMatch(doc[index].name,word);

        if (!isMatch && changeHead == 1) {
            break;
        }
        else {
            if (isMatch) {
                matches.push({
                    title: doc[index].name,
                    rep: 'artist'
                });
                changeHead = 1;
            }
        }
        index ++;
    }
    return matches;
}

function CheckMatch(name,word) {

    return name.indexOf(word) == 0 ? true : false;
}

artistRepository.prototype.addGenre = function(nameArtist,genre) {
    return new Promise(function(resolve,reject) {
        this.findByName(nameArtist)
            .then(function(doc) {
                doc.genre = array.union(doc.genre,genre);
                return doc.save();
            })
            .then(function() {
                resolve();
            });
    }.bind(this));
};

artistRepository.prototype.getCount = function() {
    return new Promise(function(resolve,reject) {
        Artist.count()
            .then(function(count) {
                resolve(count);
            });
    });
};




