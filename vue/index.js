var Vue = require('vue') ;
+var VueRouter = require('vue-router');
+var array = require('lodash');
+Vue.use(require('vue-resource'));
+Vue.use(VueRouter);
+
+var childOne = Vue.extend({
+    template: '#myinput',
+    props: ['way'],
+    methods: {
+        sendMessage: function(event) {
+            event.preventDefault();
+
+
+            this.way(this.msg);
+            this.msg = '';
+        }
+    }
+});
+
+var childTwo = Vue.extend({
+    template: '#myshow',
+    props: ['text']
+});
+
+var Parent = new Vue({
+    el: '#content',
+    components: {
+        'inputmy': childOne,
+        'show': childTwo
+    },
+    data: {
+        message: 'hello'
+    },
+    methods: {
+        getMessage: function(message) {
+            this.$set('message',message);
+        }
+    }
+});
+
+
+
