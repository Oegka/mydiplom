

module.exports = function(app) {


    app.get('/api/artist',require('controllers/api'));
    app.get('/api/album',require('controllers/findAllAlbums'));
    app.get('/addAlbumsComponents',require('controllers/addAlbumsComponents'));
    app.get('/findMatches',require('controllers/findMatches'));

    app.get('/viewAlbumContent',require('controllers/getData/ViewAlbumContent'));
    app.get('/viewArtistContent',require('controllers/getData/ViewArtistContent'));
    app.get('/viewAlbums',require('controllers/getData/viewAlbums'));
    app.get('/getAvailableGenres',require('controllers/getData/getAvailableGenres'));
    app.get('/viewTracks',require('controllers/getData/viewTracks'));
    app.get('/viewArtist',require('controllers/getData/viewArtist'));
    app.get('/getArtist',require('controllers/getData/getArtist'));

    app.options('/api/artist',require('controllers/optionsArtist'));

};