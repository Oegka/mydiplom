var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./playList.html'),
    props: ['list-music','events'],
    data: {
        show: -1
    },
    methods: {
        selectedTrack: function(index) {
            this.$set('show',index);

        },
        leavedTrack: function() {
            this.$set('show',-1);

        },
        editTrackList: function(index,e) {
            var elem = e.target.tagName == 'I' ? e.target.parentNode : e.target;
            var tmp = this.listMusic.listShowTracks[index];
            switch(elem.getAttribute('direct')) {
                case 'raise':
                    if (index -1 >= 0) {
                        this.listMusic.listShowTracks.splice(index,1,this.listMusic.listShowTracks[index-1]);
                        this.listMusic.listShowTracks.splice(index-1,1,tmp);
                    }
                    break;
                case 'lower':
                    if (index + 1 <= this.listMusic.listShowTracks.length - 1) {
                        this.listMusic.listShowTracks.splice(index,1,this.listMusic.listShowTracks[index+1]);
                        this.listMusic.listShowTracks.splice(index+1,1,tmp);
                    }

            }
        },
        deleteItemFromTrackList: function(index) {
            this.listMusic.listShowTracks.splice(index,1);
            this.leavedTrack();
        },
        showNextTracks: function() {
            this.events.showNextTracks();
        },
        transferPlayTrack: function(e,index) {
            var theSameTrack = this.listMusic.listShowTracks[index].track.name != this.$root.player.playTrack.data.track.name ? true : !this.$root.player.playTrack.isPlay;

            this.$root.events.setTrackForPlay({
                track: this.listMusic.listShowTracks[index].track,
                artist: this.listMusic.listShowTracks[index].artist,
                album:this.listMusic.listShowTracks[index].album
            });
            this.$root.changeStateTrack(theSameTrack);
            this.$root.cutTransformTrackToPlay(index);
            this.leavedTrack();
        }
    },
    filters: {
        trimTypeFile: function(value) {
            var index = value.indexOf('.mp3');
            return value.slice(0,index);
        },
        setScroll: function(value) {
            return value.slice(0,this.$els.playlist.getAttribute('amount-tracks'));

        }

    },
    watch: {
        'listMusic.listShowTracks': function(newVal,oldVal) {
            console.log(newVal);
        }
    },
    ready: function() {

    }
});