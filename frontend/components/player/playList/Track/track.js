var Vue = require('vue') ;
var array = require('lodash');


module.exports = Vue.extend({
    template:  require('./track.html'),
    props: ['track','index'],
    data: function() {
        return {
            showPanel: false
        }
    },
    methods: {
        selectedTrack: function() {
            this.$set('showPanel',true);

        },
        leavedTrack: function() {
            this.$set('showPanel',false);

        },
        editTrackList: function(e) {
            var elem = e.target.tagName == 'I' ? e.target.parentNode : e.target;
            var direct = elem.getAttribute('direct');
            this.$parent.editTrackList(this.index,direct);
        },
        deleteItemFromTrackList: function() {
            this.$parent.deleteItemFromTrackList(this.index);
            this.leavedTrack();
        },
        transferPlayTrack: function() {
            this.$parent.transferPlayTrack(this.index);
        }
    },
    filters: {
        trimTypeFile: function(value) {
            var index = value.indexOf('.mp3');
            return value.slice(0,index);
        }

    },
    watch: {

    },
    ready: function() {

    }
});