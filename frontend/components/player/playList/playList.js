var Vue = require('vue') ;
var array = require('lodash');
var style = require('./playList.css');

module.exports = Vue.extend({
    template:  require('./playList.html'),
    components: {
        'track': require('./Track/track.js')
    },
    props: ['list-music','play-track'],
    data: {

    },
    methods: {
        editTrackList: function(index,direct) {

            var tmp = this.listMusic.listShowTracks[index];
            switch(direct) {
                case 'raise':
                    if (index -1 >= 0) {
                        this.listMusic.listShowTracks.splice(index,1,this.listMusic.listShowTracks[index-1]);
                        this.listMusic.listShowTracks.splice(index-1,1,tmp);
                        this.$root.reviewIndexPlayTrack('raise');
                    }
                    break;
                case 'lower':
                    if (index + 1 <= this.listMusic.listShowTracks.length - 1) {
                        this.listMusic.listShowTracks.splice(index,1,this.listMusic.listShowTracks[index+1]);
                        this.listMusic.listShowTracks.splice(index+1,1,tmp);
                        this.$root.reviewIndexPlayTrack('lower');
                    }

            }
        },
        deleteItemFromTrackList: function(index) {
            this.listMusic.listShowTracks.splice(index,1);
            this.$root.reviewIndexPlayTrack('delete',index);
        },
        showNextTracks: function() {
            this.$root.events.showNextTracks();
        },
        transferPlayTrack: function(index) {
            var theSameTrack = this.listMusic.listShowTracks[index].track.name != this.$root.player.playTrack.data.track.name ? true : !this.$root.player.playTrack.isPlay;

            this.$root.events.setTrackForPlay({
                track: this.listMusic.listShowTracks[index].track,
                artist: this.listMusic.listShowTracks[index].artist,
                album:this.listMusic.listShowTracks[index].album
            });
            this.$root.changeIndexFromPlayTrackFromPlayList(index);
            this.$root.changeStateTrack(theSameTrack);
        }
    },
    filters: {

    },
    watch: {

    },
    ready: function() {

    }
});