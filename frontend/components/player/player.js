var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./player.html'),
    components: {
        'playtrack': require('./playTrack/playTrack.js'),
        'playlist': require('./playList/playList.js')
    },
    props: ['player','events'],
    data: {
        flag: false
    },
    methods: {

    },
    filters: {
        trimTypeFile: function(value) {
            //var index = value.length > 0 ? value.indexOf('.mp3');
            return value.slice(0,index);
        }
    },
    ready: function() {

    }
});