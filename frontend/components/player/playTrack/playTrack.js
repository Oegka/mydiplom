var Vue = require('vue') ;
var array = require('lodash');
var style = require('./playTrack.css');
var ionRangeSlider = require('ion-rangeslider');


module.exports = Vue.extend({
    template:  require('./playTrack.html'),
    props: ['play-track'],
    data: function() {
        return {
            showVolume: false,
            sliderVolume: Object,
            sliderProgress: Object,
            oldVolume: '75',
            isMute: false,
            volume: '75',
            duration: 0,
            progress: '',
            changeProgress: false
        }
    },
    methods: {
        playMusic: function() {
            if (this.playTrack.isPlay) {
                $(this.$els.jplayer).jPlayer("pause");
                this.$root.changeStateTrack(false);
            }
            else {
                $(this.$els.jplayer).jPlayer("play");
                this.$root.changeStateTrack(true);
            }
        },
        setProgress: function(val) {
            $(this.$els.jplayer).jPlayer("play",val);
            this.$set('changeProgress',false);
        },
        stopMusic: function() {
            $(this.$els.jplayer).jPlayer("pause",0);
            this.$root.changeStateTrack(false);
        },
        setMusic: function(pathToMusic) {
            $(this.$els.jplayer).jPlayer("setMedia", {
                mp3: pathToMusic
            });
            $(this.$els.jplayer).jPlayer("play");
            this.$root.changeStateTrack(true);
        },
        switchTrack: function() {
            this.$root.events.setTrackForPlay(this.$root.updatePlayTrack());
        },
        createSliderVolume: function() {
            $(this.$els.volume).ionRangeSlider({
                min: 0,
                max: 100,
                from: 75,
                type: 'single',
                hide_min_max: true,
                force_edges: true
            });
            this.sliderVolume = $(this.$els.volume).data("ionRangeSlider");
        },
        createSliderProgress: function(val) {
            $(this.$els.progress).ionRangeSlider({
                min: 0,
                max: 10,
                from: 75,
                type: 'single',
                hide_min_max: true,
                hide_from_to: true,
                force_edges: true

            });
            this.sliderProgress = $(this.$els.progress).data("ionRangeSlider");
        },
        initialPlayer: function() {
            var play = this.$els.play;
            var setInitVolume = this.changeVolume;
            $(this.$els.jplayer).jPlayer({
                ready: function () {
                    setInitVolume(75);
                },
                cssSelectorAncestor: '',
                swfPath: "/js",
                supplied: "mp3",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                remainingDuration: true,
                toggleDuration: true
            });

        },
        changeVolume: function(value) {
            $(this.$els.jplayer).jPlayer("volume", value/100);
        },
        mute: function() {

            var volume = this.isMute ? this.oldVolume : 0;
            this.volume != '' && this.$set('oldVolume',this.volume);
            this.sliderVolume.update({
                from: volume
            });
            this.changeVolume(volume);
            this.$set('isMute',!this.isMute);
        },
        showVolumeSlider: function() {
            this.$set('showVolume',!this.showVolume);
        },
        setDurationTrack: function(val) {
            this.$set('duration',val);

            var index = val.indexOf(':');
            var minutes = parseInt(val.slice(0,index));
            var seconds = parseInt(val.slice(index + 1));
            var time = (+minutes) * 60 + seconds;
            this.sliderProgress.update({
                max: time
            });
        },
        updateProgress: function(val) {
            this.$set('currentTime',val);
            var index = val.indexOf(':');
            var minutes = parseInt(val.slice(0,index));
            var seconds = parseInt(val.slice(index + 1));
            var time = minutes * 60 + seconds;
            this.sliderProgress.update({
                from: +time
            });
        },
        offsetProgress: function(e) {
            if (e.target.tagName == 'SPAN') {
                switch(e.type) {
                    case 'mousedown':
                        this.$set('changeProgress',true);
                        break;
                    case 'mouseup':
                        this.setProgress(+this.progress);
                }
            }
        },
        transferToAlbum: function() {
            this.$root.events.switchView({
                name: this.playTrack.data.album.name,
                repository: 'album'
            });
        },
        transferToArtist: function() {
            this.$root.events.switchView({
                name: this.playTrack.data.artist.name,
                repository: 'artist'
            });
        }
    },
    filters: {
        trimTypeFile: function(value) {
            if(value) {
                var index = value.indexOf('.mp3');
                return value.slice(0,index);
            }
            return value;
        },
        transformToTime: function(val) {
            return $.jPlayer.convertTime(+val)
        }
    },
    watch: {
        'playTrack.data.track.pathToTrack': function(newValue,oldValue) {
            this.setMusic(newValue);
        },
        'playTrack.isPlay': function(newValue,oldValue) {
            newValue ? $(this.$els.jplayer).jPlayer("play") : $(this.$els.jplayer).jPlayer("pause");

        },
        'volume': function(value,old) {
            if(value != '') {
                +value > 0 ? this.$set('isMute',false)  : this.$set('isMute',true);
            }
            this.changeVolume(+value);
        }
    },
    ready: function() {

    },
    attached: function() {
        this.initialPlayer();

        $(this.$els.jplayer).bind($.jPlayer.event.ended , function(event) {
            this.switchTrack();
        }.bind(this));

        $(this.$els.jplayer).bind($.jPlayer.event.durationchange , function(event) {
            this.setDurationTrack($.jPlayer.convertTime(event.jPlayer.status.duration));
        }.bind(this));

        $(this.$els.jplayer).bind($.jPlayer.event.timeupdate , function(event) {
            !this.changeProgress && this.updateProgress($.jPlayer.convertTime(event.jPlayer.status.currentTime));
        }.bind(this));

        this.createSliderVolume();
        this.createSliderProgress();
    }
});