var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./startPage.html'),
    components: {
        'album': require('./album/album.js')
    },
    props: ['data_for_view'],
    methods: {
        switchView: function(index) {

        }
    },
    attached: function() {
        this.$root.resetDataForView();
        this.$root.events.getNextContent({
            nameController: 'viewAlbums',
            sort: 'createDate',
            sortMode: 'max'
        });
    }
});