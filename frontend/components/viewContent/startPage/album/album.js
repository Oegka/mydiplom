var Vue = require('vue') ;
var style = require('./album.css');
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./album.html'),
    props: ['data','switch-view'],
    methods: {
        transferAlbum: function(event) {
            event.preventDefault();
            this.$root.events.switchView({
                name: this.data.album.name,
                repository: 'album'
            });
        },
        transferArtist: function() {
            this.$root.events.switchView({
                name: this.data.artist.name,
                repository: 'artist'
            });
        }

    },
    attached: function() {
        this.$els.image.onload = function() {
            this.$root.changeCoords(this.$root.$els.view.getBoundingClientRect().bottom);
        }.bind(this);
    }
});