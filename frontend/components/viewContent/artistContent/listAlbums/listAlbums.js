var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./listAlbums.html'),
    props: ['data_for_view','switch-view'],
    components: {
        'album': require('./album/album.js')
    },
    methods: {

    },
    ready: function() {

    }
});