var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./album.html'),
    props: ['album','switch-view'],
    methods: {
        transferAlbum: function(event) {
            event.preventDefault();

            this.$root.events.switchView({
                name: this.album.name,
                repository: 'album'
            });
        }
    }
});