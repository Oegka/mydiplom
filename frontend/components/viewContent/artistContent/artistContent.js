var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./artistContent.html'),
    props: ['data_for_view','events'],
    components: {
        'info-artist': require('./infoArtist/infoArtist.js'),
        'list-albums': require('./listAlbums/listAlbums.js')
    },
    methods: {

    },
    ready: function() {

    }
});