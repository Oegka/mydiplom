var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./content.html'),
    props: ['main','child','transfer-genre','transfer-track','transfer-another','current'],
    data: function() {

    },
    methods: {
        transferChild: function(genre) {
            switch(this.current) {
                case 'album':
                    this.transferAnother({
                        name: this.child.name,
                        repository: 'artist'
                    });
                    break;
                case 'artist':
                    this.transferGenre([genre]);
                    break;
                case 'track':
                    this.transferAnother({
                        name: this.child.name,
                        repository: 'album'
                    });
            }
        },
        transferMain: function() {
            switch(this.current) {
                case 'album':
                    this.transferAnother({
                        name: this.main.name,
                        repository: 'album'
                    });
                    break;
                case 'artist':
                    this.transferAnother({
                        name: this.main.name,
                        repository: 'artist'
                    });
                    break;
                case 'track':
                   this.transferTrack(this.main,this.child);
            }
        }
    },
    filters: {
        trimTypeFile: function(val) {
            var index = val ? val.indexOf('mp3') : -1;
            if (index > 0) {
                return val.slice(0,index - 1);
            }
            return val;
        }
    }
});