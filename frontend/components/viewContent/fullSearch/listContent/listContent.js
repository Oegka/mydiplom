var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./listContent.html'),
    components: {
        'content': require('./content/content.js')
    },
    props: ['list-content','current-content','for-transfer-content'],
    data: function() {
        return {
            childContent: {
                album: 'artist',
                track: 'album'
            }
        }
    },
    methods: {
        test: function() {
            console.log(this.listContent);
        }
    }
});