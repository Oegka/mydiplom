var Vue = require('vue') ;
var _ = require('lodash');

module.exports = Vue.extend({
    template:  require('./fullSearch.html'),
    components: {
        'list-content': require('./listContent/listContent.js'),
        'filter': require('./filter/filter.js')
    },
    props: ['data_for_view','filter-params'],
    data: function() {
        return {
            Content: {
                album: 'album',
                artist: 'artist',
                track: 'track'
            },
            currentContent: 'album',
            needController: {
                album: '/viewAlbums',
                artist: '/viewArtist',
                track: '/viewTracks'
            },
            forTransferContent: {},
            currentGenresFromDropdown: []
        }
    },
    methods: {
        switchTab: function(e) {
            this.currentGenresFromDropdown.length > 0 && this.filterParams.changeMethodInit('transferGenreBetweenTab');
            switch(e.target.getAttribute('data-tab')) {
                case 'first':
                    this.updateDataForView('album');
                    break;
                case 'second':
                    this.updateDataForView('artist');
                    break;
                case 'third':
                    this.updateDataForView('track');
            }
        },
        addContent: function(genre) {

            this.currentGenresFromDropdown.length == 0 && this.$root.resetDataForView();
            this.$root.$set('fetching.viewTab',true);

            this.$root.events.gety(this.needController[this.currentContent] + '?genre=' + genre)
                .then(function(response) {
                    this.updateContent(response);
                }.bind(this));
        },
        updateContent: function(content) {
            this.$root.dataForView.dataWithServer = _.sortBy(_.union(this.$root.dataForView.dataWithServer,content),this.currentContent + '.name');
            this.$root.$set('fetching.viewTab',false);
        },
        restoreContent: function(genres) {
            this.$root.resetDataForView();
            this.$root.$set('fetching.viewTab',true);
            console.log(this.needController[this.currentContent]);
            Promise.all(genres.map(function(genre) {
                return this.$root.events.gety(this.needController[this.currentContent] + '?genre=' + genre);
            }.bind(this)))
                .then(function(response) {
                    this.updateContent(_.flatten(response));
                }.bind(this))
        },
        updateCurrentGenres: function(newGenres) {
            this.currentGenresFromDropdown = newGenres;
        },
        updateDataForView: function(newContent) {
            this.currentContent = newContent;

            this.$root.resetDataForView();
            this.$root.$set('fetching.viewTab',true);

            switch(this.filterParams.methodInit) {
                case 'without':
                    this.$root.events.getNextContent({
                        nameController: this.needController[newContent],
                        sort: 'name',
                        sortMode: 'min'
                    },'viewTab');
                    break;
                case 'transferGenreBetweenTab':
                    this.restoreContent(this.currentGenresFromDropdown);
                    this.filterParams.changeMethodInit('without');
                    break;
            }

        },
        transferTrackFromSearch: function(track,album) {
            this.$root.events.gety('/getArtist?id=' + track.idArtist)
                .then(function(artist) {
                    this.$root.events.setTrackForPlay({
                        track: track,
                        album: album,
                        artist: artist
                    });
                }.bind(this));
        },
        setGenre: function(genre) {
            this.$root.$set('filter.chooseGenre',genre);
        },
        test: function() {
            console.log(this.filterParams.availableGenres);
        }
    },
    attached: function() {
        $('.menu .item')
            .tab({

            })
        ;
        this.filterParams.chooseGenre == '' && this.updateDataForView('album');

        this.forTransferContent = {
            track: this.transferTrackFromSearch,
            another: this.$root.events.switchView,
            genre: this.setGenre
        }
    }
});