var Vue = require('vue') ;
var _ = require('lodash');
var style = require('./filter.css');

module.exports = Vue.extend({
    template:  require('./filter.html'),
    props: ['add-content','restore-content','update-current-genres','params'],
    data: function() {
        return {
            countGenres: 0,
            isRemove: false
        }
    },
    watch: {
        'params.chooseGenre': function(val) {
            $(this.$els.genres).dropdown('set selected',val);
            this.updateCurrentGenres([val]);
            //this.$root.$set('filter.chooseGenre','');
            this.params.changeGenre('');
        }
    },
    methods: {
        settingsDropdown: function() {
            var self= this;
            $(this.$els.genres)
                .dropdown({
                    onAdd: function(addedValue) {
                        self.addContent(addedValue);
                    },
                    onRemove: function() {
                        self.$set('isRemove',true);
                    },
                    onChange: function(newValue) {
                        self.updateCurrentGenres(newValue);
                        self.isRemove && self.restoreContent(newValue);
                        self.$set('isRemove',false);
                    }
                })
            ;
        },
        test: function() {
            $(this.$els.genres).dropdown('set selected','Поп');
        }
    },
    attached: function() {
        this.settingsDropdown();

        $(this.$els.genres).dropdown('set selected',this.params.chooseGenre);
        this.params.chooseGenre != '' && this.updateCurrentGenres([this.params.chooseGenre]);
        this.params.changeGenre('');
    }
});