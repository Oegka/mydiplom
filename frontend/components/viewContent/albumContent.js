var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./albumContent.html'),
    props: ['data_for_view','events'],
    data: {
        show: -1
    },
    methods: {
        selectedTrack: function(e,index) {
            this.$set('show',index);

        },
        leavedTrack: function(e) {
            this.$set('show',-1);

        },
        transferArtist: function() {
            this.events.switchView({
                name: this.data_for_view.artist.name,
                repository: 'artist'
            });
        },
        transferTrack: function(e,index) {
            this.events.addTrackToList({
                track: this.data_for_view.tracks[index],
                artist: this.data_for_view.artist,
                album: this.data_for_view.album.name
            });
        },
        transferPlayTrack: function(e,index) {
            this.events.setTrackForPlay({
                track: this.data_for_view.tracks[index],
                artist: this.data_for_view.artist,
                album: this.data_for_view.album.name
            });

        }
    },
    filters: {
        trimTypeFile: function(value) {
            var index = value.indexOf('.mp3');
            return value.slice(0,index);
        }
    },
    ready: function() {

    }
});