var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./artistContent.html'),
    props: ['data_for_view','events'],
    components: {
        album: require('./album.js')
    },
    data: {
        show: -1
    },
    methods: {
        selectedTrack: function(e,index) {
            this.$set('show',index);

        },
        leavedTrack: function(e) {
            this.$set('show',-1);

        }
    },
    ready: function() {

    }
});