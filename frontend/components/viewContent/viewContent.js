var Vue = require('vue') ;
var array = require('lodash');
var style = require('./viewContent.css');

module.exports = Vue.extend({
    template:  require('./viewContent.html'),
    components: {
        'album_content': require('./albumContent/albumContent.js'),
        'artist_content': require('./artistContent/artistContent.js'),
        'albums_start_page': require('./startPage/startPage.js'),
        'full_search': require('./fullSearch/fullSearch.js')
    },
    props: ['data_for_view','current_view','events','filter-params'],
    methods: {

    }
});