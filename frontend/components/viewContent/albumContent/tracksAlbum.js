var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./tracksAlbum.html'),
    props: ['data_for_view','events'],
    data: {
        show: -1,
        selectTrack: {

        }
    },
    methods: {
        selectedTrack: function(e,index) {
            this.$set('show',index);

        },
        leavedTrack: function(e) {
            this.$set('show',-1);

        },
        transferTrack: function(e,index) {
            this.$root.events.addTrackToList({
                track: this.data_for_view.tracks[index],
                artist: this.data_for_view.artist,
                album: this.data_for_view.album
            });
        },
        transferPlayTrack: function(e,index) {
            var isOn = this.data_for_view.tracks[index].name != this.$root.player.playTrack.data.track.name ? true : !this.$root.player.playTrack.isPlay;
            this.$root.events.setTrackForPlay({
                track: this.data_for_view.tracks[index],
                artist: this.data_for_view.artist,
                album: this.data_for_view.album
            });

            console.log(isOn);
            this.$root.changeStateTrack(isOn);
        }
    },
    filters: {
        trimTypeFile: function(value) {
            var index = value.indexOf('.mp3');
            return value.slice(0,index);
        }
    },
    ready: function() {

    }
});