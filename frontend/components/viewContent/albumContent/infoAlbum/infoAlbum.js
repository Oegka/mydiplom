var Vue = require('vue') ;
var array = require('lodash');

module.exports = Vue.extend({
    template:  require('./infoAlbum.html'),
    props: ['album','artist','transfer'],
    methods: {
        transferArtist: function() {
            this.$root.events.switchView({
                name: this.artist.name,
                repository: 'artist'
            });
        },
        transferGenre: function(e) {
            this.$root.setView();
            this.$root.setValueInFilter(this.album.genreAlbum);
        }

    },
    ready: function() {

    }
});