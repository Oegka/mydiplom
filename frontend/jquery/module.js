
exports.search = search;

function search (app) {
    $('.ui.search')
        .search({
            apiSettings: {
                onResponse: function(response) {

                    response.item.forEach(function(match) {
                        app.nameRepository[match.title] = match.rep;
                    });
                },
                url: '/findMatches?word={query}'

            },
            fields: {
                results: 'item',
                title: 'title'
            },
            minCharacters: 3,
            error: {
                noResults: 'Исполнитель,альбом,трек не был найден'
            }
        })
    ;
}

exports.tip = tip;

function tip() {
    $('#opa')
        .popup({
            on: 'click',
            popup: '.special.popup'
        })
    ;
}


