var _ = require('lodash');
var join = require('path').join;

module.exports = new methods();

function methods() {

}

//---------------------------------------------------------------------------------------------------------------------

//Events application


methods.prototype.getContent = function(controller,app) {
    this.$http
        .get(controller)
        .then(function(response) {
            this.dataForView.dataWithServer = response.data;

        });
};

methods.prototype.addTrackToList = function(objTrack) {

    var sharedListTracks = this.player.listMusic.listShowTracks.concat(this.player.listMusic.listTracksFromTmpArea);
    var isExist = _.find(sharedListTracks,function(data) {
        return data.track.name == objTrack.track.name;
    });
    if (!isExist) {
        this.player.listMusic.amountShowTracks  > this.player.listMusic.listShowTracks.length && (this.player.listMusic.listShowTracks.push(objTrack))
        || this.player.listMusic.listTracksFromTmpArea.push(objTrack);
    }
};

methods.prototype.setTrackForPlay = function(objTrack) {
    this.$set('player.playTrack.ready',true);
    console.log(objTrack.pathToTrack);
    this.player.playTrack.data = objTrack;
};

methods.prototype.showNextTracks = function() {

    var tmp = this.player.listMusic.listTracksFromTmpArea;
    var tracksForAdd = tmp.length > 3 ? tmp.splice(0,3) : tmp.splice(0);
    this.player.listMusic.listShowTracks = _.concat(this.player.listMusic.listShowTracks,tracksForAdd);
    this.$set('player.listMusic.amountShowTracks',this.player.listMusic.amountShowTracks + 3);
};

methods.prototype.getNextContent = function(req,loader) {
    var sort = 'sort=' + req.sort;
    var sortMode = '&sortMode=' + req.sortMode;

    var head = '&head=' + this.dataForView.amount;

    var numLimit = this.dataForView.offset + this.dataForView.amount;
    var limit = '&limit=' + numLimit;

    var genre = req.genre ? ('&genre=' + req.genre) : '';
    var params = '?' + sort + sortMode + head + limit + genre;

    this.$set('dataForView.amount',numLimit);
    var controller = req.nameController + params;

    this.$http
        .get(controller)
        .then(function(response) {

            this.dataForView.dataWithServer = _.concat(this.dataForView.dataWithServer,response.data);
            var fetch = 'fetching.' + loader;
            this.$set(fetch,false);
        });
};

methods.prototype.gety = function(controller,loader) {
    return new Promise(function(resolve,reject) {
        this.$http
            .get(controller)
            .then(function(response) {
                var fetch = 'fetching.' + loader;
                this.$set(fetch,false);
                resolve(response.data);
            });
    }.bind(this));
};
