var Vue = require('vue') ;
var join = require('path').join;
var eventsMethods = require('./repository/methods');
var styles = require('./index.css');
Vue.use(require('vue-resource'));


var app = new Vue({
    el: '#content',
    template: require('./_index.html'),
    components: {
        'view_content': require('./components/viewContent/viewContent.js'),
        'player': require('./components/player/player.js')
    },
   data: function() {
       return {
           dataForView: {
               dataWithServer: [],
               offset: 12,
               amount: 0
           },
           player: {
               listMusic: {
                   listShowTracks: [],
                   listTracksFromTmpArea: [],
                   amountShowTracks: 4
               },
               playTrack: {
                   ready: false,
                   isPlay: false,
                   data: {
                       track: {},
                       album: {},
                       artist: {}
                   },
                   indexFromPlayList: -1
               }
           },
           currentView: '',
           nameRepository: {},
           events: {},
           coords: {
               componentView: 0
           },
           fetching: {
               nextContent: false,
               viewTab: false
           },
           filter: {
               chooseGenre: '',
               methodInit: 'without',
               availableGenres: [],
               changeGenre: function(newGenre) {
                   this.filter.chooseGenre = newGenre;
               }.bind(this),
               changeMethodInit: function(newMethod) {
                   this.filter.methodInit = newMethod;
               }.bind(this)
           }
       }
   },
    watch: {

    },
    methods: {
        getContent: function(e) {
            this.switchView({
                name: e.target.textContent,
                repository: this.nameRepository[e.target.textContent]
            });
        },
        switchView: function(content) {

            var userChoose = {
                caseAlbum: '/viewAlbumContent?name=',
                caseArtist: '/viewArtistContent?name='
            };

            switch(content.repository) {
                case 'artist':
                    this.$set('currentView','artist_content');
                    this.events.getContent(userChoose.caseArtist + content.name);
                    break;
                case 'album':
                    this.$set('currentView','album_content');
                    this.events.getContent(userChoose.caseAlbum + content.name);
                    break;
            }
        },
        changeStateTrack: function(state) {
            this.$set('player.playTrack.isPlay', state);
        },
        getNextTrackFromTrackList: function() {
            var fullListTracks = this.player.listMusic.listShowTracks.concat(this.player.listMusic.listTracksFromTmpArea);
            return fullListTracks[this.player.playTrack.indexFromPlayList];
        },
        changeIndexFromPlayTrackFromPlayList: function(newIndex) {
            var potentialIndex = newIndex != undefined ? newIndex : this.player.playTrack.indexFromPlayList + 1;
            var fullListTracks = this.player.listMusic.listShowTracks.concat(this.player.listMusic.listTracksFromTmpArea);
            this.$set('player.playTrack.indexFromPlayList', potentialIndex < fullListTracks.length ? potentialIndex : 0);
        },
        updatePlayTrack: function() {
            this.changeIndexFromPlayTrackFromPlayList();
            return this.getNextTrackFromTrackList();
        },
        reviewIndexPlayTrack: function(action,deleteIndex) {
            switch(action) {
                case 'raise':
                    this.$set('player.playTrack.indexFromPlayList' , this.player.playTrack.indexFromPlayList - 1);
                    break;
                case 'lower':
                    this.$set('player.playTrack.indexFromPlayList' , this.player.playTrack.indexFromPlayList + 1);
                    break;
                case 'delete':
                    var newIndex = this.player.playTrack.indexFromPlayList - 1 >= 0 ? this.player.playTrack.indexFromPlayList - 1 : -1;
                    this.player.playTrack.indexFromPlayList >= deleteIndex && this.$set('player.playTrack.indexFromPlayList' , newIndex);
            }

        },
        changeCoords: function(val) {
            this.$set('coords.componentView',val);
        },
        setView: function() {
            this.$set('currentView','full_search');
        },
        resetDataForView: function() {
            this.$set('dataForView.amount',0);
            this.$set('dataForView.dataWithServer',[]);
        },
        setValueInFilter: function(genre) {
            this.$set('filter.chooseGenre',genre);
        }
    },
    ready: function() {

        require('./jquery/module').search(this);
        require('./jquery/module').tip();
        this.events = {
            switchView: this.switchView,
            addTrackToList: eventsMethods.addTrackToList.bind(this),
            setTrackForPlay: eventsMethods.setTrackForPlay.bind(this),
            showNextTracks: eventsMethods.showNextTracks.bind(this),
            getContent: eventsMethods.getContent.bind(this),
            getNextContent: eventsMethods.getNextContent.bind(this),
            gety: eventsMethods.gety.bind(this)
        };
        this.$set('currentView','albums_start_page');

        this.events.gety('/getAvailableGenres')
            .then(function(genres) {
                this.$set('filter.availableGenres',genres);
            }.bind(this));


    },
    attached: function() {
        window.onscroll = function() {
            if (this.coords.componentView - 70  < document.documentElement.clientHeight + window.pageYOffset && !this.fetching.nextContent) {
                this.$set('fetching.nextContent',true);

                eventsMethods.getNextContent.bind(this)({
                    nameController: '/viewAlbums',
                    sort: 'createDate',
                    sortMode: 'max'
                },'nextContent');
            }
        }.bind(this);

    }
});



