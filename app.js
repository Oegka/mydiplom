var express = require('express');
var http = require('http');
var fs = require('fs');
var HttpError = require('error/HttpError');
var config=require('config');
var mongoose = require('lib/mongoose');

var app = express();

app.get('/', function(req,res) {
    sendFile('index.html',res);
});

require('./routes')(app);

/*app.get('/', function(req,res) {
    sendFile('index.html',res);
});
app.get('/api/artist',func);
app.options('/api/artist',require('controllers/optionsArtist'));*/

app.use(express.static('./public'));
app.use(express.static('./content'));

app.use(function(err, req, res, next) {
    if (typeof err == 'number') {
        err = new HttpError(err);
    }

    if (err instanceof HttpError) {
        res.send(err);
    }
    else {
        err = new HttpError(500);
        res.send(err);
    }

});

app.listen(3000, function() {
    //console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

});



function sendFile(fileName,res) {
    var fileStream = fs.createReadStream(fileName);
    fileStream.on('error',function() {
        res.statusCode = 500;
        res.end('Server error');
    });
    fileStream.on('open',function() {
        fileStream.pipe(res);
    });
}

