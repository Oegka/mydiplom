var artistRepository = require('repository/repArtist');
var albumRepository = require('repository/repAlbum');
var trackRepository = require('repository/repTrack');
var array = require('lodash');

module.exports = function(req,res,next) {
    console.log(req.query.word);
    var artist = new artistRepository();
    var album = new albumRepository();
    var track = new trackRepository();
    var matchesFull = [];
    var key = req.query.word[0].toUpperCase() + req.query.word.slice(1);

    artist.getCount()
        .then(function(count) {
            return artist.findMatches(key,{limit: count});
        })
        .then(function(matches) {
            matchesFull = array.union(matchesFull,matches);
            return '';
        })
        .then(function() {
            return album.getCount();
        })
        .then(function(count) {
            return album.findMatches(key,{limit: count});
        })
        .then(function(matches) {
            matchesFull = array.union(matchesFull,matches);
            res.end(JSON.stringify({item: matchesFull}));
        });

};