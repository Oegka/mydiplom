var ArtistRepository = require('repository/repArtist');


module.exports = function(req,res,next) {

    var params = {
        query: {},
        sort: req.query.sort || 'name',
        limit: req.query.limit || 3,
        head: req.query.head || 0,
        sortMode: req.query.sortMode || 'min'
    };


    setParamsForSearch(req.query,params)
        .then(function() {
            return findArtists(params,next);
        })
        .then(function(artistsFound) {
            res.end(JSON.stringify(artistsFound));
        })
        .catch(function(err) {
            console.log(err);
        });
};

function setParamsForSearch(query,params) {
    return new Promise(function(resolve,reject) {
        Object.keys(query).forEach(function(prop) {
            if (prop != 'sort' && prop != 'limit' && prop != 'head' && prop != 'sortMode') {
                params.query[prop] = query[prop];
            }
        });

        resolve();
    })
}

function findArtists(params,next) {

    var artists = new ArtistRepository();

    return artists.findAll(params,next);
}