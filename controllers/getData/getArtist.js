var artistRepository = require('repository/repArtist');

module.exports = function(req,res,next) {

    var artist = new artistRepository();
    artist.findById(req.query.id)
        .then(function(doc) {
            res.end(JSON.stringify(doc));
        });
};