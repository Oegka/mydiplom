var trackRepository = require('repository/repTrack');
var albumRepository = require('repository/repAlbum');

module.exports = function(req,res,next) {
    var params = {
        query: {},
        sortMode: req.query.sortMode || 'min',
        sort: req.query.sort || 'name',
        head: req.query.head || 0,
        limit: req.query.limit || 12
    };

    if (req.query.genre) {
        params.query.genre = req.query.genre;
    }

    var Track = new trackRepository();
    var Album = new albumRepository();

    Track.findAll(params)
        .then(function(tracks) {
            console.log(tracks);
            return bindTracksWithAlbums(tracks,Album);
        })
        .then(function(response) {
            res.end(JSON.stringify(response));
        })

};

function bindTracksWithAlbums(tracks,repAlbum) {
    return new Promise(function(resolve,reject) {
        Promise.all(tracks.map(function(track) {
            return repAlbum.findById(track.album);
        }))
            .then(function(doc) {
                resolve(createResponseObject(tracks,doc));
            });
    });
}

function createResponseObject(tracks,albums) {
    var count = -1;
    var responseObject = tracks.map(function(track) {
        count ++;
        return {
            track: {
                name: track.name,
                pathToTrack: track.pathToTrack,
                idArtist: track.artist
            },
            album: {
                name: albums[count].name,
                pathToImage: albums[count].pathToImage
            }
        }
    });

    return responseObject;
}