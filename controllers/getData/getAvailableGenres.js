var paramsRepository = require('repository/parametersRep');

module.exports = function(req,res,next) {
    var params = new paramsRepository();
    params.getGenres()
        .then(function(genres) {
            res.end(JSON.stringify(genres));
        });

};