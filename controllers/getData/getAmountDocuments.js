var artistRepository = require('repository/repArtist');
var albumRepository = require('repository/repAlbum');

var reps = {
    artist: new artistRepository(),
    album: new albumRepository()
};

module.exports = function(req,res,next) {
    var repository = reps[req.query.rep];
    repository.getCount({})
        .then(function(doc) {
            res.end(JSON.stringify(doc));
        });

};