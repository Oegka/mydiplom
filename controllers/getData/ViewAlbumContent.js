var AlbumRepository = require('repository/repAlbum');
var ArtistRepository = require('repository/repArtist');
var TrackRepository = require('repository/repTrack');
var lodash = require('lodash');

module.exports = function(req,res,next) {
    var album = new AlbumRepository();
    var artist = new ArtistRepository();
    var track = new TrackRepository();
    var response = {
        album: {},
        artist: {},
        tracks: []
    };
    var idAlbum = '';
    var nameAlbum = req.query.name;

    album.findByName(nameAlbum)
        .then(function(doc) {
            response.album = doc;
            idAlbum = doc._id;

            return doc.artist;
        })
        .then(function(idArtist) {

            return artist.findById(idArtist);
        })
        .then(function(docArtist) {

            response.artist = docArtist;

            return docArtist._id;
        })
        .then(function(artistId) {

            return track.findAll({query: {
                album: idAlbum
            }});
        })
        .then(function(docTracks) {

            response.tracks = lodash.union(response.tracks,docTracks);
            res.end(JSON.stringify(response));
        })
        .catch(function(err) {
            console.log(err);
        });

};