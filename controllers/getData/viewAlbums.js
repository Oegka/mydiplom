var AlbumRepository = require('repository/repAlbum');
var ArtistRepository = require('repository/repArtist');

module.exports = function(req,res,next) {
    var params = {
        query: {},
        sortMode: req.query.sortMode || 'min',
        sort: req.query.sort || 'name',
        head: req.query.head || 0,
        limit: req.query.limit || 12
    };

    if (req.query.genre) {
        params.query.genreAlbum = req.query.genre;
    }

    var album = new AlbumRepository();
    var artist = new ArtistRepository();

    album.findAll(params,next)
        .then(function(doc) {
            return bindAlbumsWithArtists(doc,artist);
        })
        .then(function(doc) {

            res.end(JSON.stringify(doc));
        });
};

function bindAlbumsWithArtists(albums,repArtist) {
    return new Promise(function(resolve,reject) {
        Promise.all(albums.map(function(album) {
            return repArtist.findById(album.artist);
        }))
            .then(function(doc) {
                resolve(createResponseObject(albums,doc));
            })
    });
}

function createResponseObject(albums,artists) {
    var count = -1;
    var responseObject = albums.map(function(album) {
        count ++;
        return {
            album: {
                name: album.name,
                pathToImage: album.pathToImage
            },
            artist: {
                name: artists[count].name,
                pathToImage: artists[count].pathToImage
            }
        }
    });

    return responseObject;
}