var artistRepository = require('repository/repArtist');

module.exports = function(req,res,next) {

    var params = {
        query: {},
        sort: req.query.sort || 'name',
        limit: req.query.limit || 12,
        head: req.query.head || 0,
        sortMode: req.query.sortMode || 'min'
    };

    if(req.query.genre) {
        params.query.genre = req.query.genre;
    }
    var Artist = new artistRepository();

    Artist.findAll(params)
        .then(function(artists) {
            return artists.map(function(artist) {
                return {
                    artist: {
                        name: artist.name,
                        genre: artist.genre,
                        pathToImage: artist.pathToImage
                    }
                }
            });
        })
        .then(function(response) {
            res.end(JSON.stringify(response));
        })

};