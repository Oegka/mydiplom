var AlbumRepository = require('repository/repAlbum');
var ArtistRepository = require('repository/repArtist');
var lodash = require('lodash');

module.exports = function(req,res,next) {
    var album = new AlbumRepository();
    var artist = new ArtistRepository();
    var response = {
        album: {},
        artist: {}
    };

    var nameArtist = req.query.name;

    artist.findByName(nameArtist)
        .then(function (doc) {
            response.artist = doc;
            return doc._id;
        })
        .then(function (idArtist) {

            return album.findAll({query: {
                artist: idArtist
            }});
        })
        .then(function (docAlbums) {
            response.album = lodash.union(response.album,docAlbums);
            res.end(JSON.stringify(response));
        })
        .catch(function (err) {
            console.log(err);
        });

};