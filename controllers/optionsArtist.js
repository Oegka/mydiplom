var ArtistRepository = require('repository/repArtist');
var array = require('lodash');

module.exports = function(req,res,next) {

    var artist = new ArtistRepository();
    var option = {
        attribute: [],
        methods: []
    };

    setOptionAttribute(option,artist)
        .then(function() {

            return setOptionMethods(option);
        })
        .then(function() {
            var json = JSON.stringify(option);

            res.end(json);
        });


};

function setOptionAttribute(option,artist) {

    return artist.getAttribute()
        .then(function(arrayAttributes) {

            option.attribute = array.cloneDeep(arrayAttributes);
            return Promise.resolve();
        });
}

function setOptionMethods(option) {

    return getMethods()
        .then(function(arrayMethods) {

            option.methods = array.cloneDeep(arrayMethods);
            return Promise.resolve();
        });
}

function getMethods() {


    return Promise.all(Object.keys(ArtistRepository.prototype).map(function(methods) {

        return {
            name: methods,
            amountArguments: methods.length
        };
    }));
}