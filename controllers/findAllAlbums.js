var AlbumRepository = require('repository/repAlbum');


module.exports = function(req,res,next) {

    var params = {
        query: {},
        sort: req.query.sort || 'name',
        sortMode: req.query.sortMode || 'min'
    };

    setParamsForSearch(req.query,params)
        .then(function() {
            return findAlbums(params,next);
        })
        .then(function(albumsFound) {
            res.end(JSON.stringify(albumsFound));
        })
};

function setParamsForSearch(query,params) {
    return Promise.all([Object.keys(query).forEach(function(key) {
        if (key != 'sort' && key != 'limit' && key != 'sortMode') {
            params.query[key] = query[key];
            Promise.resolve(params.query[key]);
        }
    })]);
}

function findAlbums(params,next) {

    var album = new AlbumRepository();

    return album.findAll(params,next);
}