var artistRepository = require('repository/repArtist');
var albumRepository = require('repository/repAlbum');
//var flex= require('repository/flexie');
var lodash = require('lodash');

module.exports = function(req,res,next) {

    var params = {
        query: {},
        sort: 'name',
        head: req.query.head || 0,
        limit: req.query.limit || 6
    };

    getAlbums(params)
        .then(function(listAlbums) {

            return getObjectArtists(listAlbums);
        })
        .then(function(dataForAlbumElement) {

            res.end(JSON.stringify(dataForAlbumElement));
        })
        .catch(function(err) {
            console.log('er');
            throw err;
        });

};

function getAlbums(params) {

    var album = new albumRepository();
    return album.findAll(params);
}

function getListArtistsById(listAlbums) {

    var repArtist = new artistRepository();

    return Promise.all(listAlbums.map(function(album) {
        return repArtist.findById(album.artist);
    }));
}

function getObjectArtists(listAlbums) {
    return new Promise(function(resolve,reject) {
        getListArtistsById(listAlbums)
            .then(function(listArtist) {

                var objectArtists = createObjectArtist(listAlbums,listArtist);

                resolve(objectArtists);
            })
            .catch(function(err) {

                console.log(err);
            });
    });
}

function createObjectArtist(listAlbums,listArtist) {
    var count = -1;


    return listArtist.map(function() {
        count++;
        return {
            album: listAlbums[count],
            artist: {
                name: listArtist[count].name,
                pathToImage: listArtist[count].pathToImage
            }
        };
    });

}

