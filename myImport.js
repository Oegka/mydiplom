var fs = require('repository/fsPromise').fsPromise;
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var Task = require('class/Task');


var objRootDirs = {
    IMPORT_DIR: 'insertData',
    CONTENT_MUSIC: 'content',
    fullIMPORT_DIR: __dirname + '/insertData',
    fullCONTENT_MUSIC: __dirname + '/content'
};

importFiles();

/**
 * Работа основывается на том,что каждой папке в директории импорта создается своя область работы т.е. задача.
 * Каждая задача работает только со своей папкой.
 *
 * @returns {*}
 */
function importFiles() {
    return async(function() {
        var groupTasks = await(createGroupTasks());

        await(taskStartCreatingFoldersToContentMusic(groupTasks));
        await(taskStartUpdateDatabase(groupTasks));
        await(upgradeParamsCollections(groupTasks));
    })();
}

function createGroupTasks() {
    return async(function() {
        var foldersInImportDir = await(fs.readdir(objRootDirs.fullIMPORT_DIR));

        return await(foldersInImportDir.map(function(nameFolder) {
            return new Task(nameFolder,objRootDirs);
        }));

    })();
}

function taskStartCreatingFoldersToContentMusic(groupTask) {
    return async(function() {

        return await(groupTask.map(function(task) {
            return task.createDirsToContentMusic();
        }));

    })();
}

function taskStartUpdateDatabase(groupTask) {
    return async(function() {

        return await(groupTask.map(function(task) {
            return task.updateDatabase();
        }));

    })();
}

function upgradeParamsCollections(groupTasks) {
    var genre = groupTasks.map(function(task) {
        return task._objAlbum.genre[0];
    });

    genre.length > 0 && groupTasks[0].updateParamsCollections(genre);
}

