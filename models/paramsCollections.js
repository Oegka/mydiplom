var mongoose = require('mongoose');
var array = require('lodash');

Parameters = new mongoose.Schema({
    genres: {
        type: Array,
        unique: true,
        require: true
    },
    countArtists: {
        type: Number,
        unique: true,
        require: true
    },
    countAlbums: {
        type: Number,
        unique: true,
        require: true
    },
    countTracks: {
        type: Number,
        unique: true,
        require: true
    }
});

module.exports = mongoose.model('Parameters',Parameters);
