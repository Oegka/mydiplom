var mongoose = require('mongoose');
var array = require('lodash');

var Album=mongoose.Schema({
        name: {
            type: String,
            unique: true,
            require: true
        },
        DateOfIssue: {
            type: Date,
            unique: false,
            require: true
        },
        genreAlbum: {
            type: String,
            unique: false,
            require: false
        },
        artist: {
            type: String,
            unique: false,
            require: true
        },
        pathToImage: {
            type: String
        },
        createDate: {
            type: Date
        }
    }

);

Album.statics.sort = function(params,docs) {
    if (params.sortMode == 'min') {
        return Promise.resolve(array.sortBy(docs,params.sort));
    }
    else {
        return Promise.resolve(array.sortBy(docs,params.sort).reverse());
    }

};

Album.statics.limit = function(params,doc) {
    return Promise.resolve(doc.slice(params.head,params.limit));
};
module.exports = mongoose.model("Album",Album);
