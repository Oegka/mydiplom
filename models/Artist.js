var mongoose = require('mongoose');
var array = require('lodash');


var Artist = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    genre: {
        type: [],
        unique: false,
        require: true
    },
    pathToImage: {
        type: String,
        require: true
    }
});

//---------------------------------------------------------------------------------------------------------------------
Artist.statics.sort = function(params,docs) {

    if (params.sortMode == 'min') {
        return Promise.resolve(array.sortBy(docs,params.sort));
    }
    else {
        return Promise.resolve(array.sortBy(docs,params.sort).reverse());
    }
};
//---------------------------------------------------------------------------------------------------------------------
Artist.statics.limit = function(params,doc) {
    return Promise.resolve(doc.slice(params.head,params.limit));
};
//---------------------------------------------------------------------------------------------------------------------
module.exports = mongoose.model('Artist',Artist);