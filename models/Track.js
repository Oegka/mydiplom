var mongoose = require('mongoose');
var array = require('lodash');

Track = new mongoose.Schema({
    name: {
        type: String
    },
    pathToTrack: {
        type: String,
        unique: true,
        require: true
    },
    artist: {
        type: String
    },
    album: {
        type: String
    },
    genre: {
        type: String
    }
});

Track.statics.sort = function(params,docs) {
    if (params.sortMode == 'min') {
        return Promise.resolve(array.sortBy(docs,params.sort));
    }
    else {
        return Promise.resolve(array.sortBy(docs,params.sort).reverse());
    }
};

Track.statics.limit = function(params,doc) {
    return Promise.resolve(doc.slice(params.head,params.limit));
};

module.exports = mongoose.model('Track',Track);