var fs = require('fs');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var ArtistRepository = require('repository/repArtist');
var AlbumRepository = require('repository/repAlbum');
var TrackRepository = require('repository/repTrack');
var array = require('lodash');
var config = require('config');
var mongoose = require('lib/mongoose');
var ncp = require('ncp').ncp;
var IMPORT_DIR = '/insertData';
var CONTENT_MUSIC = '/content';

var arrayNewFiles = [];
var objDir = {
    path: '',
    nameDir: String,
    futurePath: '',
    majorDir: IMPORT_DIR
};


myImportFilesInContentMusic(arrayNewFiles,objDir);
//---------------------------------------------------------------------------------------------------------------------
/**
 * Импортирует файлы из временных папок находящихся
 * в папке IMPORT_DIR. Так же заполняет директориями
 * папку CONTENT_MUSIC
 *
 * @param arrayNewFiles
 * массив файлов полученных из временной папки,в дальнейшем с помощью него
 * будет заполняться база данных
 *
 * @param odjDir
 * объект директории, который включает в себя
 * -Путь до директории/файла
 * -имя директории директории/файла
 * -путь до директории куда копировать
 * -имя основной папки
 *
 * @returns {*}
 */

function myImportFilesInContentMusic(arrayNewFiles,odjDir) {
    return async(function() {

        await(creatingNewDirectoriesInContentMusic(arrayNewFiles,odjDir));

        await(addingDataToBase(arrayNewFiles));


    })();
}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Создает новые директории в CONTENT_MUSIC(где хранится музыка)
 *
 * @param arrayNewFiles
 * @param odjDir
 * @returns {*}
 */

function creatingNewDirectoriesInContentMusic(arrayNewFiles,odjDir) {
    return async(function() {
        await(callForEachElement(arrayNewFiles,odjDir,processingOfFiles));
    })();
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Предназначение функции вызвать колбэк для каждого элемента директории
 *
 * @param arrayNewFiles
 * @param objDir
 * @param callback
 * Функция, которую нужно применить к каждому элементу директории
 *
 * @returns {*}
 * Возвращает результат колбэка
 */

function callForEachElement(arrayNewFiles,objDir,callback) {

    return async(function() {

        var dirContents = await(readdirReturnPromise(objDir)) ;
        return await(dirContents.map(function(file) {

            return callback(arrayNewFiles,{
                nameDir: file,
                path: objDir.path + '/' + file,
                majorDir: IMPORT_DIR
            });
        }));

    })();
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Нужна для предворительной обработки, то есть  разпарсить имя директории на артиста,альбом и т.д
 *
 * @param arrayNewFiles
 * @param objDir
 * @returns {*}
 *
 */

function processingOfFiles(arrayNewFiles,objDir) {

    return async(function() {
        var objFile = await(createObjectForInsertToBase(objDir));
        objFile.track = await(sampleFiles(objDir,objFile));
        arrayNewFiles.push(objFile);
        return '';
    })();
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Создает объект файла, который состоит из имя артиста,имя,даты и жанра альбома и массива треков
 *
 * @param objDir
 * @returns {*}
 * Возвращает объект
 */

function createObjectForInsertToBase(objDir) {
    return async(function() {
        var value = objDir.nameDir.split('_');

        objDir['futurePath'] = '';


        return {
            artist: {
                newProp: false,
                name: value[0],
                genre: [value[3]]
            },
            album: {
                newProp: false,
                name: value[1],
                date: new Date(value[2],1,1),
                genre: [value[3]],
                artist: value[0]
            },
            track: []
        };
    })();
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * В этой функции уже идет перемещение,удаление и создание директорий
 *
 * @param objDir
 * @param objFile
 * @returns {*}
 * Возвращается объект директории,он нам нужен так как он хранит нвоые пути до треков
 */

function sampleFiles(objDir,objFile) {

    return async(function() {
       Object.keys(objFile).forEach(function(key) {

            return await(createDirInContentMusic(objDir,objFile,key));

        });

        var objTracks = await(getNewPathForTracks(objDir,objFile));

        await(moveTracksInContentMusic(objDir));
        await(deleteTemporaryDir(objDir));

        return objTracks;
    })();
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Получаем путь до треков уже в CONTENT_MUSIC
 *
 * @param objDir
 * @returns {*}
 * Возвращает объект директории ,в котором хранятся пути
 */

function getNewPathForTracks(objDir,objFile) {
    return async(function() {

        var nameTracks = await(callForEachElement([],objDir,function(array,objDir) {
             return objDir.nameDir;
        }));

        return await(nameTracks.map(function(track) {

            return {
                name: track,
                pathToTrack: CONTENT_MUSIC + '/' + objDir.futurePath.slice(0,-1) + '/' + track,
                artist: objFile.artist.name,
                album: objFile.album.name
            }
        }));


    })();
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Создаются директории полученные из парсинга названия временной папки в директории
 * IMPORT_DIR. Если уже такая директория есть в CONTENT_MUSIC, то она не создается
 *
 * @param objDir
 * @param objFile
 * @param key
 * Ключи в объекте файла, т.е. артист,альбом,треки
 * @returns {*}
 */

function createDirInContentMusic(objDir,objFile,key) {
    return async(function() {

        if (key != 'track') {

            objDir.futurePath = await(objDir.futurePath +  objFile[key].name);

            !await(existsReturnPromise(objDir.futurePath)) && await(addDir(objDir.futurePath,objFile[key]));
            objDir.futurePath = objDir.futurePath + '/';
        }
        return '';

    })();
}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Добавляет директорию в CONTENT_MUSIC
 *
 * @param path
 * @returns {*}
 */

function addDir(path,obj) {
    obj.newProp = true;
    return mkdirReturnPromise(path);

}

//---------------------------------------------------------------------------------------------------------------------

function readdirReturnPromise(objDir) {

    return new Promise(function(resolve,reject) {
        fs.readdir(__dirname + objDir.majorDir + objDir.path,function(err,item) {
            if (err) reject(err);
            resolve(item);
        });
    });
}

//---------------------------------------------------------------------------------------------------------------------

function mkdirReturnPromise(path) {

    return new Promise(function(resolve,reject) {
        fs.mkdir(__dirname + CONTENT_MUSIC + '/' + path,function() {

            resolve();
        });
    });
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Копирует треки из IMPORT_DIR/.../track в CONTENT_MUSIC/.../track
 *
 * @param objDir
 * @returns {Promise}
 */

function moveTracksInContentMusic(objDir) {
    return new Promise(function(resolve,reject) {
        ncp(__dirname + IMPORT_DIR + '/' + objDir.nameDir,__dirname + CONTENT_MUSIC + '/' + objDir.futurePath, function (err) {
            if (err) {
                reject(err);
            }
            resolve();
        });
    });
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Удлаяет временную папку со всем содержимым из IMPORT_DIR
 *
 * @param objDir
 * @returns {Promise}
 */

function deleteTemporaryDir(objDir) {
    await(callForEachElement([],objDir,function(arrayNewFiles,objDir) {
        return deleteFile(objDir);
    }));

    return new Promise(function(resolve,reject) {
        fs.rmdir(__dirname + objDir.majorDir + objDir.path,function(err) {
            if (err) reject(err);
            resolve();
        });
    });
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * Удаляет все файлы из нужной директории
 *
 * @param objDir
 * @returns {Promise}
 */

function deleteFile(objDir) {
    return new Promise(function(resolve,reject) {
        fs.unlink(__dirname + objDir.majorDir + objDir.path,function(err) {
            if (err) reject(err);
            resolve();
        });
    });
}
//---------------------------------------------------------------------------------------------------------------------

function existsReturnPromise(path) {
    return new Promise(function(resolve,reject) {
        fs.exists(__dirname + CONTENT_MUSIC + '/' + path,function(ex) {

            resolve(ex);
        });
    });
}
//---------------------------------------------------------------------------------------------------------------------

function statReturnPromise(path) {
    return new Promise(function(resolve,reject) {
        fs.stat(__dirname + IMPORT_DIR +  path,function(err,item) {
            if (err) reject(err);

            resolve(item);
        });
    });
}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Добавляет данные к базе
 * @param arrayNewFiles
 * Массив новых данных для добавления в базу
 * @returns {*}
 */
function addingDataToBase(arrayNewFiles) {
    return async(function() {
        var repositories = {
            artist: new ArtistRepository(),
            album: new AlbumRepository(),
            track: new TrackRepository()
        };
        await(arrayNewFiles.map(function(dataForUpdate) {
            return updateDatabase(dataForUpdate,repositories,false);
        }));
        return 'finished';
    })();

}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Обновляет базу
 * @param dataForUpdate
 * Данные для обновления. Является объектом с ключами artist,album,track
 * @param repositories
 * Массив репозиториев для работы с коллекциями
 * @param isTrack
 * Переменная показывающая идет ли обновление для коллекции треков.
 * @returns {*}
 */

function updateDatabase(dataForUpdate,repositories,isTrack) {

    return async(function() {
        return Object.keys(repositories).map(function(currentCollectionForFiling) {

            var objProperty = isTrack ? {
                nameModel: 'track',
                dataForCurrentModel: repositories[currentCollectionForFiling],
                currentRep: dataForUpdate.currentRep,
                reps: dataForUpdate.reps
            } : {
                nameModel: currentCollectionForFiling,
                dataForCurrentModel: dataForUpdate[currentCollectionForFiling],
                currentRep: repositories[currentCollectionForFiling],
                reps: repositories
            };

            currentCollectionForFiling != 'track' ? await(filing(objProperty)) : function() {
                var objTmp = {
                    reps: repositories,
                    currentRep: objProperty.currentRep
                };

                await(updateDatabase(objTmp,objProperty.dataForCurrentModel,true));
            }();

            return 'end';
        });

    })();
}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Осущесвтляется заполнение базы текущей коллекцией из переменной dataForUpdate
 * @param objProperty
 * Объект хранящий информацию о свойствах заполняемой коллекции
 * nameModel - имя модели ,в которую производится запись
 * dataForCurrentModel - данные для текущей коллекции для заполнения
 * currentRep - репозиторий текущей коллекции
 * reps - массив репозиториев
 * @returns {*}
 */
function filing(objProperty) {
    return async(function() {
        var createCollection = await(getNameFunctionForCurrentCollection('create',objProperty.nameModel));

        await(replacingTheNameOnId(objProperty.dataForCurrentModel,objProperty.reps));

        if(objProperty.dataForCurrentModel.newProp || objProperty.nameModel == 'track') {
            var funcCreateCurrentCollection = objProperty.currentRep[createCollection];

            await(funcCreateCurrentCollection(objProperty.dataForCurrentModel));
        }

        return 'end';

    })();
}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Используется для получения ключа,по которому можно вызвать интересующуй нас функцию из репозитория текущей коллекции
 * @param name
 * Имя функции
 * @param collection
 * Текущая коллекция
 * @returns {string}
 * Возвращает ключ
 */

function getNameFunctionForCurrentCollection(name,collection) {
    return name + collection[0].toUpperCase() + collection.slice(1);
}
//---------------------------------------------------------------------------------------------------------------------
/**
 * Используется для того,чтобы в данных для добавления в базу,изменить имя артиста и имя альбома на их id
 * @param objCollection
 * Объект данных для заполнения,в текущую коллекцию
 * @param repositories
 * Массив репозиториев
 * @returns {*}
 */

function replacingTheNameOnId(objCollection,repositories) {
    return async(function() {
        Object.keys(objCollection).forEach(function(key) {
            if(key == 'artist' || key == 'album') {
                var getId = getNameFunctionForCurrentCollection('getId',key);

                var repForGetId = repositories[key];
                var funcGetId = repForGetId[getId].bind(repForGetId);
                var id = await(funcGetId(objCollection[key]));

                objCollection[key] = id;
            }
        });
        return 'end';
    })();
}